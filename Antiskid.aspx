﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Antiskid.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Antiskid Series</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Ap-Blue.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Ap-Blue.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SC As Ap Blue</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Black.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Black.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Black</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Blue.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Blue.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Blue</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-C-Cloudy.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-C-Cloudy.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As C Cloudy</h4>

                            </div>
                        </div>
                    </div>

                    <!-- items -->

                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->

                    <aside class="col-md-3">

                        <h3>Available Series300 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Stone.aspx"><i class="fa fa-circle-o"></i>Stone Series</a></li>
                            <li><a href="Spcrystal.aspx"><i class="fa fa-circle-o"></i>S Sp Crystal Stone Series</a></li>
                            <li><a href="Maxcio.aspx"><i class="fa fa-circle-o"></i>Maxcio Series</a></li>
                            <li><a href="Hyper.aspx"><i class="fa fa-circle-o"></i>Hyper Series</a></li>
                            <li><a href="GlassIvory.aspx"><i class="fa fa-circle-o"></i>Glossy Ivory Series</a></li>
                            <li><a href="Glossybhama.aspx"><i class="fa fa-circle-o"></i>Glossy Bhama Series</a></li>
                            <li><a href="Coin.aspx"><i class="fa fa-circle-o"></i>Coin Series</a></li>
                            <li><a href="Arc.aspx"><i class="fa fa-circle-o"></i>Arc Series</a></li>
                            <li><a href="SPlaincolour.aspx"><i class="fa fa-circle-o"></i>Sp Plain Colour Series</a></li>
                            <li><a href="Sspcoin.aspx"><i class="fa fa-circle-o"></i>S Sp Coin Series</a></li>
                            <li><a href="Plaincolour.aspx"><i class="fa fa-circle-o"></i>Plain Colour Series</a></li>
                            <li><a href="Mattwhite.aspx"><i class="fa fa-circle-o"></i>Matt White Series</a></li>
                            <li><a href="Glossywooden.aspx"><i class="fa fa-circle-o"></i>Glossy Wooden Series</a></li>
                            <li><a href="GlossyBrown.aspx"><i class="fa fa-circle-o"></i>Glossy Brown Series</a></li>
                            <li><a href="Checkres.aspx"><i class="fa fa-circle-o"></i>Checkres Series</a></li>
                            <li><a href="Aqua.aspx"><i class="fa fa-circle-o"></i>Aqua Series</a></li>
                            <li><a href="Rustic.aspx"><i class="fa fa-circle-o"></i>Rustic Series</a></li>
                            <li><a href="OvalStone.aspx"><i class="fa fa-circle-o"></i>Oval Stone Series</a></li>
                            <li><a href="Mattivory.aspx"><i class="fa fa-circle-o"></i>Matt Ivory Series</a></li>
                            <li><a href="Glossywhite.aspx"><i class="fa fa-circle-o"></i>Glossy White Series</a></li>
                            <li><a href="Glossyblack.aspx"><i class="fa fa-circle-o"></i>Glossy Black Series</a></li>
                            <li><a href="Crystalstone.aspx"><i class="fa fa-circle-o"></i>Crystal Stone Series</a></li>
                            <li><a href="Barlino.aspx"><i class="fa fa-circle-o"></i>Barlino Series</a></li>
                            <li><a href="Antiskid.aspx"><i class="fa fa-circle-o"></i>Antiskid Series</a></li>

                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->

                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Chery-Brown.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Chery-Brown.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Chery Brown</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Gray.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Gray.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SC As Gray</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Green.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Green.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Green</h4>

                            </div>
                        </div>
                    </div>




                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Ivory.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Ivory.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Ivory</h4>

                            </div>
                        </div>
                    </div>






                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Magenta.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Magenta.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Magenta</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Oasis-Green.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Oasis-Green.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Oasis Green</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Pink.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Pink.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Pink</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-Tera-Cotta.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-Tera-Cotta.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As Tera Cotta</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="300x300/Antiskidseries/SC-As-White.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="300x300/Antiskidseries/SC-As-White.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC As White</h4>

                            </div>
                        </div>
                    </div>

                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->





                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

