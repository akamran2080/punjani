﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Glossy Black Series.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Glossy Black Series</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40004.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40004.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40004</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40006.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40006.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40006</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC60008.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC60008.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC60008</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40010.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40010.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40010</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->

                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->
                    <aside class="col-md-4">

                        <h3>Available Series 400X400 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Vitro Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy White Series</a></li>
                            <li><a href="Satin Matt White Series.aspx"><i class="fa fa-circle-o"></i>Satin Matt White Series</a></li>
                            <li><a href="Plain Cloudy Series.aspx"><i class="fa fa-circle-o"></i>Plain Cloudy Series</a></li>
                            <li><a href="Glossy Wooden Series.aspx"><i class="fa fa-circle-o"></i>Glossy Wooden Series</a></li>
                            <li><a href="Glossy Black Series.aspx"><i class="fa fa-circle-o"></i>Glossy Black Series</a></li>
                            <li><a href="Vitro Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy Ivory Series</a></li>
                            <li><a href="Satin Matt Ivory Seriesaspx"><i class="fa fa-circle-o"></i>Satin Matt Ivory Series</a></li>
                            <li><a href="Matt White Ivory Series.aspx"><i class="fa fa-circle-o"></i>Matt White Ivory Series</a></li>
                            <li><a href="Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Glossy White Series</a></li>
                            <li><a href="Glossy Bhama Series.aspx"><i class="fa fa-circle-o"></i>Glossy Bhama Series</a></li>
                            <li><a href="Sp Satin Matt and Wooden Series.aspx"><i class="fa fa-circle-o"></i>Sp Satin Matt and Wooden Series</a></li>
                            <li><a href="Rustic Series.aspx"><i class="fa fa-circle-o"></i>Rustic Series</a></li>
                            <li><a href="Leather Series.aspx"><i class="fa fa-circle-o"></i>Leather Series</a></li>
                            <li><a href="Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Glossy Ivory Series</a></li>
                            <li><a href="Galaxy Brown Series.aspx"><i class="fa fa-circle-o"></i>Galaxy Brown Series</a></li>
                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40012.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40012.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40012</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40017.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40017.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40017</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40021.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40021.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40021</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40022.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40022.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40022</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40023.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40023.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40023</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40024.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40024.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40024</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40028.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40028.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40028</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40030.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40030.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40030</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40031.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40031.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40031</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC40036.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC40036.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC40036</h4>

                            </div>
                        </div>
                    </div>


                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

