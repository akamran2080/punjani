﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Tabletop.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Table Top</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-CELLO.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-CELLO.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-CELLO</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-HALF-MOON.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong>
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-HALF-MOON.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-HALF-MOON</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-LIMCA.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-LIMCA.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-LIMCA</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-LINEA.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong>
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-LINEA.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-LINEA</h4>

                            </div>
                        </div>
                    </div>

                    <!-- items -->

                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->

                    <aside class="col-md-3">

                        <h3>Sanitarywares</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Stone.aspx"><i class="fa fa-circle-o"></i>Accessories</a></li>
                            <li><a href="Spcrystal.aspx"><i class="fa fa-circle-o"></i>Antique Craft Basin Wih Pedestal</a></li>
                            <li><a href="Maxcio.aspx"><i class="fa fa-circle-o"></i>Antique Craft Couple Suite</a></li>
                            <li><a href="Hyper.aspx"><i class="fa fa-circle-o"></i>Antique Craft Table Top</a></li>
                            <li><a href="GlassIvory.aspx"><i class="fa fa-circle-o"></i>Antique Craft Wall Mounted</a></li>
                            <li><a href="Glossybhama.aspx"><i class="fa fa-circle-o"></i>Counter Basin</a></li>
                            <li><a href="Coin.aspx"><i class="fa fa-circle-o"></i>Couple Suite</a></li>
                            <li><a href="Arc.aspx"><i class="fa fa-circle-o"></i>Couplue Suites White</a></li>
                            <li><a href="SPlaincolour.aspx"><i class="fa fa-circle-o"></i>EWC S Vitrossa</a></li>
                            <li><a href="Sspcoin.aspx"><i class="fa fa-circle-o"></i>One Piece</a></li>
                            <li><a href="Plaincolour.aspx"><i class="fa fa-circle-o"></i>Pan and Urinals</a></li>
                            <li><a href="Mattwhite.aspx"><i class="fa fa-circle-o"></i>Plain Suites</a></li>
                            <li><a href="Glossywooden.aspx"><i class="fa fa-circle-o"></i>Printed Collection</a></li>
                            <li><a href="GlossyBrown.aspx"><i class="fa fa-circle-o"></i>Stargold Vitrosa Collection</a></li>
                            <li><a href="Checkres.aspx"><i class="fa fa-circle-o"></i>Table Top</a></li>
                            <li><a href="Aqua.aspx"><i class="fa fa-circle-o"></i>Wall Hung</a></li>
                            <li><a href="Rustic.aspx"><i class="fa fa-circle-o"></i>Wall Mounted</a></li>
                            <li><a href="Washbasin.aspx"><i class="fa fa-circle-o"></i>Wash Basin</a></li>
                            <li><a href="Washrani.aspx"><i class="fa fa-circle-o"></i>Wash Basin RANI</a></li>
                            <li><a href="Washbasinped.aspx"><i class="fa fa-circle-o"></i>Wash Basin With Pedestal</a></li>
                            <li><a href="Watercloset.aspx"><i class="fa fa-circle-o"></i>Water Closet</a></li>

                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->

                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-NEW-TORUS.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-NEW-TORUS.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-NEW-TORUS</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-TORUS.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-TORUS.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-TORUS</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/SL-VIVA.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/SL-VIVA.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-VIVA</h4>

                            </div>
                        </div>
                    </div>
                   
                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->





                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

