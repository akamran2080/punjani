﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
//using System.Transactions;
using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Web;
using System.Runtime.InteropServices;

/// <summary>
/// Summary description for LoginManager
/// </summary>
public class LoginManager
{
    #region core ***********************************************
    public enum DBReturnCode
    {
        SUCCESS = 0,
        CONNECTIONFAILURE = -1,
        SUCCESSNORESULT = -2,
        SUCCESSNOAFFECT = -3,
        DUPLICATEENTRY = -4,
        EXCEPTION = -5,
        INVALIDXML = -11,
        INPUTPARAMETEREMPTY = -6,
        UNIQUEKEYVIOLATION = -7,
        REFERENCECONFLICT = -8,
        NOQUESTIONS = -9,
        INTERMISSION = -10,
        TESTCOMPLETED = -12,
        TESTTIMEOUT = -13,
        QUESTIONALREADYANSWERED = -14,
        TESTNOTFOUND = -15
    }
    public enum CountryReturnCode
    {
        SUCCESS = 0,
        EXCEPTION = -1,
        NORECORDSFOUND = -2,
        COUNTRYNOTFOUND = -3,
        COUNTRYEXISTS = -4,
    }

    private static SqlConnection OpenConnection()
    {
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PunjaniConnectionString"].ConnectionString);
            conn.Open();
        }
        catch
        {
            conn = null;
        }
        return conn;
    }
    private static void CloseConnection(SqlConnection conn)
    {
        try
        {
            conn.Close();
            conn.Dispose();
        }
        catch { }
    }
    private static DBReturnCode ExecuteQuery(string sQuery, out DataTable dtResult)
    {
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        dtResult = null;
        SqlDataAdapter dataAdapter = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                dataAdapter = new SqlDataAdapter(sQuery, conn);
                DataSet dsTmp = new DataSet();
                dataAdapter.Fill(dsTmp);
                dtResult = dsTmp.Tables[0];
                if (dtResult.Rows.Count == 0)
                    return DBReturnCode.SUCCESSNORESULT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                retcode = DBReturnCode.EXCEPTION;
            }
            finally
            {
                dataAdapter.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }
    private static DBReturnCode ExecuteNonQuery(string sQuery)
    {
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        SqlCommand cmd = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                cmd = new SqlCommand(sQuery, conn);
                if (cmd.ExecuteNonQuery() == 0)
                    return DBReturnCode.SUCCESSNOAFFECT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627 || ex.Number == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                {
                    retcode = DBReturnCode.DUPLICATEENTRY;
                }
                else
                    retcode = DBReturnCode.EXCEPTION;
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }
    private static DBReturnCode ExecuteScaler(string sQuery, out Int64 nRecordID)
    {
        nRecordID = 0;
        /*HttpContext.Current.Response.Write(sQuery);
        HttpContext.Current.Response.Write("<br/>");*/
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        SqlCommand cmd = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                sQuery += "; SELECT SCOPE_IDENTITY();";
                cmd = new SqlCommand(sQuery, conn);
                nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                if (nRecordID <= 0)
                    return DBReturnCode.SUCCESSNOAFFECT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch //(Exception e)
            {
                retcode = DBReturnCode.EXCEPTION;
                //HttpContext.Current.Response.Write(e.Message); 
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }

    #region " DB Opration With Transection "

    public static DBReturnCode ExecuteQueryT(SqlConnection Conn, String sSQL, out DataTable dtResult)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        dtResult = null;

        try
        {
            using (SqlCommand Cmd = new SqlCommand(sSQL, Conn))
            {
                using (SqlDataAdapter da = new SqlDataAdapter(sSQL, Conn))
                {
                    da.Fill(dtResult);

                    if (dtResult.Rows.Count > 0)
                        retCode = DBReturnCode.SUCCESS;
                    else
                        retCode = DBReturnCode.SUCCESSNORESULT;
                }
            }
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {

        }

        return retCode;
    }

    public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;
            Cmd.CommandText = SQL;

            if (Cmd.ExecuteNonQuery() == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.Number == 2627) // PRIMARY KEY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;
            Cmd.CommandText = SQL;

            foreach (SqlParameter par in ParameterList)
            {
                if (par != null)
                    Cmd.Parameters.Add(par);
            }

            if (Cmd.ExecuteNonQuery() == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, out Int64 RecordID, out String ErrorMessage)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        RecordID = 0;
        ErrorMessage = String.Empty;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;

            SQL += "; SELECT SCOPE_IDENTITY();";

            Cmd.CommandText = SQL;

            RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

            if (RecordID == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
            ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
            ErrorMessage += "\n\r " + SQL;

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList, out Int64 RecordID, out String ErrorMessage)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        RecordID = 0;
        ErrorMessage = String.Empty;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;

            SQL += "; SELECT SCOPE_IDENTITY();";

            Cmd.CommandText = SQL;

            foreach (SqlParameter par in ParameterList)
            {
                if (par != null)
                    Cmd.Parameters.Add(par);
            }

            RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

            if (RecordID == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
            ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
            ErrorMessage += "\n\r " + SQL;

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }
    #endregion " DB Opration With Transection "

    #endregion core

    public static DBReturnCode Admin_Login(string sUserName, string sPassword)
    {
        DBReturnCode retCOde = DBReturnCode.EXCEPTION;
       
        DataTable dtResult;
        try
        {
            string SQL = "SELECT * FROM tbl_Admin WHERE sEmailID='" + sUserName.Replace("'", "''") + "' AND sPassword='" + sPassword.Replace("'", "''") + "'";
            retCOde = LoginManager.ExecuteQuery(SQL, out dtResult);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return retCOde;
    }
}