﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for ImageHandler
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ImageHandler : System.Web.Services.WebService {

    public ImageHandler () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string GetRandomNo()
    {
        string VisaCode = "Punjani-" + GenerateRandomNumber();
        string logoFileName = VisaCode;
        Session["RandomNo"] = logoFileName;

        return logoFileName;

    }
    [WebMethod(EnableSession = true)]
    public static int GenerateRandomNumber()
    {
        int rndnumber = 0;
        Random rnd = new Random((int)DateTime.Now.Ticks);
        rndnumber = rnd.Next();


        return rndnumber;
        //string rndstring = "";
        //bool IsRndlength = false;
        //Random rnd = new Random((int)DateTime.Now.Ticks);
        //if (IsRndlength)
        //    length = rnd.Next(4, length);
        //for (int i = 0; i < length; i++)
        //{
        //    int toss = rnd.Next(1, 10);
        //    if (toss > 5)
        //        rndstring += (char)rnd.Next((int)'A', (int)'Z');
        //    else
        //        rndstring += rnd.Next(0, 9).ToString();
        //}
        //return rndstring;
    }
    
}
