﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for ProductUpdateHandler
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ProductUpdateHandler : System.Web.Services.WebService {

    public ProductUpdateHandler () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string escapejsondata(string data)
    {
        return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }

    [WebMethod(true)]
    public string showAllCategories()
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        StringBuilder jsonString = new StringBuilder();
        StringBuilder jsonStringR = new StringBuilder();
        StringBuilder jsonStringC = new StringBuilder();
        DataTable dtResult = new DataTable();
        DataManager.DBReturnCode retCode = DataManager.Change_GetSubCategory(0, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            if (dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonStringR.Append("{");
                    jsonStringR.Append("\"nID\":\"" + escapejsondata(dr["nID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sCategoryName\":\"" + escapejsondata(dr["sCategoryName"].ToString().Replace("\r\n", " ")) + "\",");
                    if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                    jsonStringR.Append("},");

                    DataTable dtCategory;
                    if (DataManager.Change_GetSubCategory(Convert.ToInt64(dr["nID"]), out dtCategory) == DataManager.DBReturnCode.SUCCESS)
                    {
                        if (dtCategory.Rows.Count > 0)
                        {
                            foreach (DataRow drA in dtCategory.Rows)
                            {
                                jsonStringC.Append("{");
                                jsonStringC.Append("\nID\"\":\"" + escapejsondata(drA["nID"].ToString().Replace("\r\n", " ")) + "\",");
                                jsonStringC.Append("\"sCategoryName\"\":\"" + escapejsondata(drA["sCategoryName"].ToString().Replace("\r\n", " ")) + "\",");

                                jsonStringC.Append("}");
                            }
                            dtCategory.Dispose();
                        }
                    }
                }
                if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                if (jsonStringC.Length > 0) jsonStringC.Remove(jsonStringC.Length - 1, 1);
                jsonString.Append("{\"Session\":\"1\",\"IsData\":\"1\",\"retCode\":\"1\",\"Topics\":[" + jsonStringR + "],\"Test\":[" + jsonStringC + "]}");
            }
        }
        else
        {
            jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
        }

        return jsonString.ToString();
    }

    [WebMethod(true)]
    public string getsAllCategories(Int64 ParentId)
    {
        StringBuilder jsonString = new StringBuilder();
        StringBuilder jsonStringR = new StringBuilder();
        StringBuilder jsonStringC = new StringBuilder();

        DataTable dtResult = new DataTable();
        DataManager.DBReturnCode retCode = DataManager.Change_GetSubCategory(ParentId, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            if (dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonStringR.Append("{");
                    jsonStringR.Append("\"nID\":\"" + escapejsondata(dr["nID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sCategoryName\":\"" + escapejsondata(dr["sCategoryName"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringC.Append(getsAllCategories(Convert.ToInt64(dr["nID"])));
                    if (jsonStringC.Length != 0)
                    {
                        jsonStringR.Append("\"sSubcategory\":[" + jsonStringC + "],");
                        jsonStringC.Clear();
                    }
                    if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                    jsonStringR.Append("},");
                }
                if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);

                jsonString.Append("{");
                jsonString.Append("\"Category\":[" + jsonStringR + "]");
                jsonString.Append("}");
            }
        }
        return jsonString.ToString();
    }

    [WebMethod(true)]
    public string addProduct(Int64 nID, string productName, string productDescription, string sManufacturerPartNumber, string fileUpload)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }

        string Imagename1 = HttpContext.Current.Session["RandomNo"].ToString();

        //string ImgName1, ImgName2, ImgName3;

        if (fileUpload != "")
        {

            fileUpload = Imagename1 + "_1.jpg";
        }

        DataManager.DBReturnCode retCode = DataManager.AddProduct(nID, productName, productDescription, sManufacturerPartNumber, fileUpload);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]
    public string SetUpdateProduct(Int64 prID, Int64 nID, string productName, long productPrice, string productDescription, string searchKeywords, string sManufacturerPartNumber, bool bMenLatest, bool bWomenLatest, bool bKidsLatest)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        DataManager.DBReturnCode retCode = DataManager.SetUpdateProduct(prID, nID, productName, productPrice, productDescription, searchKeywords, sManufacturerPartNumber, bMenLatest, bWomenLatest, bKidsLatest);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]
    public string getProductSummary(Int64 nCatID)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        DataTable dtResult = new DataTable();
        StringBuilder jsonStringR = new StringBuilder();
        StringBuilder jsonString = new StringBuilder();
        DataManager.DBReturnCode retCode = DataManager.getProductSummary(nCatID, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            if (dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonStringR.Append("{");
                    jsonStringR.Append("\"nID\":\"" + escapejsondata(dr["nID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nCategoryID\":\"" + escapejsondata(dr["nCategoryID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sName\":\"" + escapejsondata(dr["sProductName"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sManufacturerNumber\":\"" + escapejsondata(dr["sManufacturerNumber"].ToString().Replace("\r\n", " ")) + "\",");
                    if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                    jsonStringR.Append("},");
                }
                if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"1\",\"SategorySummary\":[" + jsonStringR + "]}");
            }
        }
        else
        {
            jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
        }

        return jsonString.ToString();
    }

    [WebMethod(true)]
    public string DeleteProduct(Int64 nID)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        DataManager.DBReturnCode retCode = DataManager.DeleteProduct(nID);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]

    public string getUpdateProduct(Int64 nID)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        StringBuilder jsonString = new StringBuilder();
        StringBuilder jsonStringR = new StringBuilder();
        DataTable dtResult = new DataTable();
        DataManager.DBReturnCode retCode = DataManager.getUpdateProduct(nID, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            if (dtResult.Rows.Count > 0)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonStringR.Append("{");
                    jsonStringR.Append("\"nID\":\"" + escapejsondata(dr["nID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nCategoryID\":\"" + escapejsondata(dr["nCategoryID"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sProductName\":\"" + escapejsondata(dr["sProductName"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sProductPrice\":\"" + escapejsondata(dr["sProductPrice"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sProductDescription\":\"" + escapejsondata(dr["sProductDescription"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sSearchKeywords\":\"" + escapejsondata(dr["sSearchKeywords"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sAdminKeywords\":\"" + escapejsondata(dr["sAdminKeywords"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sManufacturerNumber\":\"" + escapejsondata(dr["sManufacturerNumber"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nGin\":\"" + escapejsondata(dr["nGin"].ToString().Replace("\r\n", " ")) + "\",");
                    //jsonStringR.Append("\"nAdditionalCharge\":\"" + escapejsondata(dr["nAdditionalCharge"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nWeight\":\"" + escapejsondata(dr["nWeight"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nLength\":\"" + escapejsondata(dr["nLength"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"sWidth\":\"" + escapejsondata(dr["sWidth"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"nHeight\":\"" + escapejsondata(dr["nHeight"].ToString().Replace("\r\n", " ")) + "\",");
                    //jsonStringR.Append("\"sDeliverydates\":\"" + escapejsondata(dr["sDeliverydates"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"bMenLatest\":\"" + escapejsondata(dr["bMenLatest"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"bWomenLatest\":\"" + escapejsondata(dr["bWomenLatest"].ToString().Replace("\r\n", " ")) + "\",");
                    jsonStringR.Append("\"bKidsLatest\":\"" + escapejsondata(dr["bKidsLatest"].ToString().Replace("\r\n", " ")) + "\",");
                    if (jsonStringR.Length > 0) jsonStringR.Remove(jsonStringR.Length - 1, 1);
                    jsonStringR.Append("},");
                }
                jsonString.Append("{\"Session\":\"1\",\"retCode\":\"1\",\"CategorySummary\":[" + jsonStringR + "]}");
            }

        }
        else
        {
            jsonString.Append("{\"Session\":\"1\",\"retCode\":\"0\"}");
        }
        return jsonString.ToString();

    }
    
}
