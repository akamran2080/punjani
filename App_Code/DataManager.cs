﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
/// <summary>

/// Summary description for DataManager
/// </summary>
public class DataManager
{
    public DataManager()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region core ***********************************************
    public enum DBReturnCode
    {
        SUCCESS = 0,
        CONNECTIONFAILURE = -1,
        SUCCESSNORESULT = -2,
        SUCCESSNOAFFECT = -3,
        DUPLICATEENTRY = -4,
        EXCEPTION = -5,
        INVALIDXML = -11,
        INPUTPARAMETEREMPTY = -6,
        UNIQUEKEYVIOLATION = -7,
        REFERENCECONFLICT = -8,
        NOQUESTIONS = -9,
        INTERMISSION = -10,
        TESTCOMPLETED = -12,
        TESTTIMEOUT = -13,
        QUESTIONALREADYANSWERED = -14,
        TESTNOTFOUND = -15
    }
    public enum CountryReturnCode
    {
        SUCCESS = 0,
        EXCEPTION = -1,
        NORECORDSFOUND = -2,
        COUNTRYNOTFOUND = -3,
        COUNTRYEXISTS = -4,
    }

    private static SqlConnection OpenConnection()
    {
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PunjaniConnectionString"].ConnectionString);
            conn.Open();
        }
        catch
        {
            conn = null;
        }
        return conn;
    }
    private static void CloseConnection(SqlConnection conn)
    {
        try
        {
            conn.Close();
            conn.Dispose();
        }
        catch { }
    }
    private static DBReturnCode ExecuteQuery(string sQuery, out DataTable dtResult)
    {
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        dtResult = null;
        SqlDataAdapter dataAdapter = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                dataAdapter = new SqlDataAdapter(sQuery, conn);
                DataSet dsTmp = new DataSet();
                dataAdapter.Fill(dsTmp);
                dtResult = dsTmp.Tables[0];
                if (dtResult.Rows.Count == 0)
                    return DBReturnCode.SUCCESSNORESULT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                retcode = DBReturnCode.EXCEPTION;
            }
            finally
            {
                dataAdapter.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }
    private static DBReturnCode ExecuteNonQuery(string sQuery)
    {
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        SqlCommand cmd = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                cmd = new SqlCommand(sQuery, conn);
                if (cmd.ExecuteNonQuery() == 0)
                    return DBReturnCode.SUCCESSNOAFFECT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627 || ex.Number == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                {
                    retcode = DBReturnCode.DUPLICATEENTRY;
                }
                else
                    retcode = DBReturnCode.EXCEPTION;
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }
    private static DBReturnCode ExecuteScaler(string sQuery, out Int64 nRecordID)
    {
        nRecordID = 0;
        /*HttpContext.Current.Response.Write(sQuery);
        HttpContext.Current.Response.Write("<br/>");*/
        DBReturnCode retcode = DBReturnCode.SUCCESS;
        SqlCommand cmd = null;
        SqlConnection conn = OpenConnection();
        if (conn == null)
        {
            retcode = DBReturnCode.CONNECTIONFAILURE;
        }
        else
        {
            try
            {
                sQuery += "; SELECT SCOPE_IDENTITY();";
                cmd = new SqlCommand(sQuery, conn);
                nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                if (nRecordID <= 0)
                    return DBReturnCode.SUCCESSNOAFFECT;
                else
                    return DBReturnCode.SUCCESS;
            }
            catch //(Exception e)
            {
                retcode = DBReturnCode.EXCEPTION;
                //HttpContext.Current.Response.Write(e.Message); 
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(conn);
            }
        }
        return retcode;
    }

    #region " DB Opration With Transection "

    public static DBReturnCode ExecuteQueryT(SqlConnection Conn, String sSQL, out DataTable dtResult)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        dtResult = null;

        try
        {
            using (SqlCommand Cmd = new SqlCommand(sSQL, Conn))
            {
                using (SqlDataAdapter da = new SqlDataAdapter(sSQL, Conn))
                {
                    da.Fill(dtResult);

                    if (dtResult.Rows.Count > 0)
                        retCode = DBReturnCode.SUCCESS;
                    else
                        retCode = DBReturnCode.SUCCESSNORESULT;
                }
            }
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {

        }

        return retCode;
    }

    public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;
            Cmd.CommandText = SQL;

            if (Cmd.ExecuteNonQuery() == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.Number == 2627) // PRIMARY KEY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;
            Cmd.CommandText = SQL;

            foreach (SqlParameter par in ParameterList)
            {
                if (par != null)
                    Cmd.Parameters.Add(par);
            }

            if (Cmd.ExecuteNonQuery() == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, out Int64 RecordID, out String ErrorMessage)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        RecordID = 0;
        ErrorMessage = String.Empty;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;

            SQL += "; SELECT SCOPE_IDENTITY();";

            Cmd.CommandText = SQL;

            RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

            if (RecordID == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
            ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
            ErrorMessage += "\n\r " + SQL;

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }

    public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList, out Int64 RecordID, out String ErrorMessage)
    {
        DBReturnCode retCode = DBReturnCode.SUCCESS;
        SqlCommand Cmd = null;
        RecordID = 0;
        ErrorMessage = String.Empty;
        try
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.Transaction = Tran;

            SQL += "; SELECT SCOPE_IDENTITY();";

            Cmd.CommandText = SQL;

            foreach (SqlParameter par in ParameterList)
            {
                if (par != null)
                    Cmd.Parameters.Add(par);
            }

            RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

            if (RecordID == 0)
                retCode = DBReturnCode.SUCCESSNOAFFECT;
            else
                retCode = DBReturnCode.SUCCESS;

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.Message);
            ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
            ErrorMessage += "\n\r " + SQL;

            if (ex.Number == 2627) // PRIMARY KEY VIOLATION
            {
                retCode = DBReturnCode.DUPLICATEENTRY;
            }
            else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
            {
                retCode = DBReturnCode.UNIQUEKEYVIOLATION;
            }
            else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
            {
                retCode = DBReturnCode.REFERENCECONFLICT;
            }
            else
            {
                retCode = DBReturnCode.EXCEPTION;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (Cmd != null)
                Cmd.Dispose();
        }

        return retCode;
    }
    #endregion " DB Opration With Transection "

    #endregion core

    public static DBReturnCode Admin_Login(string sUserName, string sPassword, out Admin objAdmin)
    {
        DBReturnCode retCOde = DBReturnCode.EXCEPTION;
        objAdmin = new Admin();
        DataTable dtResult;
        try
        {
            string SQL = "SELECT * FROM tbl_Admin WHERE sEmailID='" + sUserName.Replace("'", "''") + "' AND sPassword='" + sPassword.Replace("'", "''") + "'";
            retCOde = DataManager.ExecuteQuery(SQL, out dtResult);
            if (retCOde == DataManager.DBReturnCode.SUCCESS)
            {
                objAdmin.nID = Convert.ToInt64(dtResult.Rows[0]["nID"]);
                objAdmin.sName = dtResult.Rows[0]["sName"].ToString();
                objAdmin.sEmailID = dtResult.Rows[0]["sEmailID"].ToString();
                objAdmin.sPassword = dtResult.Rows[0]["sPassword"].ToString();
                dtResult.Dispose();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return retCOde;
    }

    public static DBReturnCode ShowCategory_OnLoad(out DataTable dtResult)
    {
        dtResult = null;
        string SQL = "SELECT * FROM tbl_ProductCategories WHERE nCategoryParentID = 0";
        return DataManager.ExecuteQuery(SQL, out dtResult);

    }

    public static DBReturnCode ShowProduct_OnLoad(out DataTable dtResult)
    {
        dtResult = null;
        string SQL = "SELECT sProductName, nID FROM tbl_Products";
        return DataManager.ExecuteQuery(SQL, out dtResult);

    }

    public static DBReturnCode Change_GetSubCategory(Int64 sCategoryProductId, out DataTable dtResult)
    {
        dtResult = null;
        string SQL = "SELECT * FROM tbl_ProductCategories WHERE nCategoryParentID=" + sCategoryProductId;
        return DataManager.ExecuteQuery(SQL, out dtResult);
    }

    public static DBReturnCode Add_Category(string sCategory)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        try
        {
            string SQL = "INSERT INTO tbl_ProductCategories (sCategoryName,nCategoryParentID) values('" + sCategory + "','" + 0 + "')";
            retCode = DataManager.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return retCode;
    }

    public static DBReturnCode Add_SubCategory(string subCategory, Int64 mainCategory)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        try
        {
            string SQL = "INSERT INTO tbl_ProductCategories (sCategoryName, nCategoryParentID) values('" + subCategory + "','" + mainCategory + "')";
            retCode = DataManager.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return retCode;
    }

    public static DBReturnCode DeleteCategory(string sParentCategory)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        try
        {
            string SQL = "DELETE FROM tbl_ProductCategories WHERE nID=" + sParentCategory + "OR nCategoryParentID=" + sParentCategory;
            retCode = DataManager.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return retCode;
    }

    public static DBReturnCode showAllCategories(out DataTable dtResult)
    {
        dtResult = null;
        string sSql = "";
        return DataManager.ExecuteQuery(sSql, out dtResult);
    }

    public static DBReturnCode AddProduct(Int64 nID, string productName, string productDescription, string sManufacturerPartNumber, string fileUpload)
    {        
        // to get file name from address
        //string[] pathArr = fileUpload.Split('\\');
        //string fileName = pathArr.Last().ToString();

        //string[] pathArr1 = image1.Split('\\');
        //string fileName1 = pathArr1.Last().ToString();

        //string[] pathArr2 = image2.Split('\\');
        //string fileName2 = pathArr2.Last().ToString();
        
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        string sSQL = "INSERT INTO tbl_Products (nCategoryID,sProductName,sProductDescription,sManufacturerNumber,sProductImage) VALUES ('" + nID + "','" + productName + "','" +
            productDescription + "','" + sManufacturerPartNumber + "','" + fileUpload + "')";
        retCode = DataManager.ExecuteNonQuery(sSQL);
        return retCode;
    }

    public static DBReturnCode SetUpdateProduct(Int64 prID, Int64 nID, string productName, long productPrice, string productDescription, string searchKeywords, string sManufacturerPartNumber, bool bMenLatest, bool bWomenLatest, bool bKidsLatest)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        string sSQL = "UPDATE tbl_Products SET nCategoryID='" + nID + "',sProductName='" + productName + "', sProductPrice='" + productPrice + "',sProductDescription='" + productDescription + "',sSearchKeywords='" + searchKeywords + "',sManufacturerNumber='" + sManufacturerPartNumber + "',bMenLatest='" + bMenLatest + "',bWomenLatest='" + bWomenLatest + "',bKidsLatest='" + bKidsLatest + "' WHERE nID=" + prID;
        retCode = DataManager.ExecuteNonQuery(sSQL);
        return retCode;
    }

    public static DBReturnCode getProductSummary(Int64 nCatID, out DataTable dtResult)
    {
        dtResult = null;
        string sSql = "select * from tbl_Products where nCategoryID=" + nCatID;
        return DataManager.ExecuteQuery(sSql, out dtResult);
    }

    public static DBReturnCode getProductComment(Int64 nID, out DataTable dtResult)
    {
        dtResult = null;
        string sSql = "select Comment, Product_id from tbl_Comment where Product_id=" + nID;
        return DataManager.ExecuteQuery(sSql, out dtResult);
    }

    public static DBReturnCode DeleteProduct(Int64 nID)
    {
        DBReturnCode retCode = DBReturnCode.EXCEPTION;
        string sSql = "DELETE FROM tbl_Products WHERE nID=" + nID;
        retCode = DataManager.ExecuteNonQuery(sSql);
        return retCode;
    }

    public static DBReturnCode getUpdateProduct(Int64 nID, out DataTable dtResult)
    {
        dtResult = null;
        string sSql = "SELECT * FROM tbl_Products WHERE nID=" + nID;
        return DataManager.ExecuteQuery(sSql, out dtResult);
    }
}
   