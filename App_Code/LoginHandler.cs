﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for LoginHandler
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class LoginHandler : System.Web.Services.WebService {

    public LoginHandler () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string escapejsondata(string data)
    {
        return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }

    [WebMethod(true)]
    public string UserLogin(string sUserName, string sPassword)
    {
        string jsonString = "";
        
        LoginManager.DBReturnCode retCode = LoginManager.Admin_Login(sUserName, sPassword);
        if (retCode == LoginManager.DBReturnCode.SUCCESS)
        {
           
            jsonString = "{\"retCode\":\"1\"}";
        }
        else
        {
            jsonString = "{\"retCode\":\"0\"}";
        }
        return jsonString;
    }
    
}
