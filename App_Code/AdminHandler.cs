﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for AdminHandler
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class AdminHandler : System.Web.Services.WebService {

    public AdminHandler () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string escapejsondata(string data)
    {
        return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }

    [WebMethod(true)]
    public string UserLogin(string sUserName, string sPassword)
    {
        string jsonString = "";
        Admin objAdmin = new Admin();
        DataManager.DBReturnCode retCode = DataManager.Admin_Login(sUserName, sPassword, out objAdmin);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            Session["ADMIN"] = objAdmin;
            jsonString = "{\"retCode\":\"1\"}";
        }
        else
        {
            jsonString = "{\"retCode\":\"0\"}";
        }
        return jsonString;
    }


    [WebMethod(EnableSession = true)]
    public string User_SessionTimeout()
    {
        Session["ADMIN"] = null;
        string sJsonString = "{\"Session\":\"0\"}";
        return sJsonString;
    }

    [WebMethod(true)]
    public string Add_Category(string sCategory)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }

        DataManager.DBReturnCode retCode = DataManager.Add_Category(sCategory);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }


    [WebMethod(true)]
    public string Add_SubCategory(string subCategory, Int64 mainCategory)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        DataManager.DBReturnCode retCode = DataManager.Add_SubCategory(subCategory, mainCategory);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    private string escapejosndata(string data)
    {
        return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }

    [WebMethod(true)]
    public string ShowCategory_OnLoad()
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        try
        {
            string jsonString = "";
            DataTable dtResult = null;
            DataManager.DBReturnCode retCode = DataManager.ShowCategory_OnLoad(out dtResult);
            if (retCode == DataManager.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Category\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            dtResult.Dispose();
            return jsonString;
        }
        catch
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]
    public string ShowProduct_OnLoad()
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        try
        {
            string jsonString = "";
            DataTable dtResult = null;
            DataManager.DBReturnCode retCode = DataManager.ShowProduct_OnLoad(out dtResult);
            if (retCode == DataManager.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Product\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            dtResult.Dispose();
            return jsonString;
        }
        catch
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]
    public string getProductSummary(Int64 nID)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        string jsonString;
        DataTable dtResult = new DataTable();
        StringBuilder jsonStringR = new StringBuilder();
        //StringBuilder jsonString = new StringBuilder();
        DataManager.DBReturnCode retCode = DataManager.getProductComment(nID, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            jsonString = "";
            if (dtResult.Rows.Count > 0)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Product\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
        }
        else
        {
            jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        return jsonString;
    }

    [WebMethod(true)]
    public string Change_GetSubCategory(Int64 sSubCategory)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        //if (sSubCategory == )
        //{
        //    return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //}
        string jsonString = "";
        DataTable dtResult = null;
        DataManager.DBReturnCode retCode = DataManager.Change_GetSubCategory(sSubCategory, out dtResult);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            if (dtResult.Rows.Count > 0)
            {
                DataView dv = dtResult.DefaultView;
                dv.Sort = "nCategoryParentID ASC";
                dtResult = dv.ToTable();
            }

            DataTable dtChart = new DataTable();
            dtChart.Columns.Add("ID");
            dtChart.Columns.Add("nID");
            dtChart.Columns.Add("sSubCategoryName");
            Int32 nCount = 1;

            foreach (DataRow dr in dtResult.Rows)
            {
                DataRow drNew = dtChart.NewRow();
                drNew["ID"] = nCount.ToString();
                drNew["nID"] = dr["nID"].ToString();
                drNew["sSubCategoryName"] = dr["sCategoryName"].ToString();
                nCount++;
                dtChart.Rows.Add(drNew);
            }

            if (dtChart.Rows.Count > 0)
            {
                DataView dv = dtChart.DefaultView;
                dv.Sort = "ID ASC";
                dtChart = dv.ToTable();
            }
            foreach (DataRow drc in dtChart.Rows)
            {
                jsonString += "{";
                foreach (DataColumn dc in dtChart.Columns)
                {
                    jsonString += "\"" + dc.ColumnName + "\":\"" + escapejsondata(drc[dc].ToString().Replace("\r\n", " ")) + "\",";
                }
                jsonString = jsonString.Trim(',') + "},";
            }
            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"SubCategory\":[" + jsonString.Trim(',') + "]}";
            dtChart.Dispose();
        }
        else
        {
            jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
        dtResult.Dispose();
        return jsonString;
    }

    [WebMethod(true)]
    public string DeleteCategory(string sParentCategory)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }
        if (sParentCategory == "")
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
        DataManager.DBReturnCode retCode = DataManager.DeleteCategory(sParentCategory);
        if (retCode == DataManager.DBReturnCode.SUCCESS)
        {
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        }
        else
        {
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
    }

    [WebMethod(true)]
    public string getProductDetailonChanegCategory(Int64 nCategoryID)
    {
        if (!Utils.ValidateSession())
        {
            return "{\"Session\":\"0\"}";
        }

        return "";
    }

    [WebMethod(true)]
    public string Analysis(string Comment)
    {
        var request = (HttpWebRequest)WebRequest.Create("http://sentiment.vivekn.com/api/text/");

        string postData = "txt=" + Comment;
        var data = Encoding.ASCII.GetBytes(postData);

        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;

        using (var stream = request.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }

        var response = (HttpWebResponse)request.GetResponse();

        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        string jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ret_Result\":[" + responseString.Trim(',') + "]}";
        return jsonString;
    }

    [WebMethod(EnableSession = true)]
    public string CheckSession()
    {
        if (Session["ADMIN"] != null)
            return "{\"Session\":\"1\",\"retCode\":\"1\"}";
        else
            return "{\"Session\":\"1\",\"retCode\":\"0\"}";
    }
    
}
