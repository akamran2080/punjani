﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

/// <summary>
/// Summary description for TilesHandler
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class TilesHandler : System.Web.Services.WebService {

    public TilesHandler () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string escapejosndata(string data)
    {
        return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }

    [WebMethod]
    public string Products(Int32 nID)
    {
        string jsonString = "";

        DataTable dtResult;
        TilesManager.DBReturnCode retcode = TilesManager.Products(nID, out dtResult);
        if (retcode == TilesManager.DBReturnCode.SUCCESS)
        {
            jsonString = "";
            foreach (DataRow dr in dtResult.Rows)
            {
                jsonString += "{";
                foreach (DataColumn dc in dtResult.Columns)
                {
                    jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                }
                jsonString = jsonString.Trim(',') + "},";
            }
            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Products\":[" + jsonString.Trim(',') + "]}";
            dtResult.Dispose();
        }
        else
        {
            jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
        return jsonString;
    }

    [WebMethod]
    public string SideMenu(Int32 nID)
    {
        string jsonString = "";

        DataTable dtResult;
        TilesManager.DBReturnCode retcode = TilesManager.SideMenu(nID, out dtResult);
        if (retcode == TilesManager.DBReturnCode.SUCCESS)
        {
            jsonString = "";
            foreach (DataRow dr in dtResult.Rows)
            {
                jsonString += "{";
                foreach (DataColumn dc in dtResult.Columns)
                {
                    jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                }
                jsonString = jsonString.Trim(',') + "},";
            }
            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tbl_Menu\":[" + jsonString.Trim(',') + "]}";
            dtResult.Dispose();
        }
        else
        {
            jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }
        return jsonString;
    }
}
