﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 90px;">

        <!-- PAGE TITLE -->
        <header id="page-title" class="nopadding">
            <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d465.1370654787694!2d79.11622193494001!3d21.148556661674935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1422952535791" width="100%" height="450" frameborder="0"></iframe>
            <script type="text/javascript">
                var $googlemap_latitude = -37.812344,
                    $googlemap_longitude = 144.968900,
                    $googlemap_zoom = 13;
            </script>
        </header>

        <section id="contact" class="container">


            <div class="row">

                <!-- FORM -->
                <div class="col-md-8">

                    <h2>Drop us a line or just say <strong><em>Hello!</em></strong></h2>

                    <!-- 
							if you want to use your own contact script, remove .hide class
						-->

                    <!-- SENT OK -->
                    <div id="_sent_ok_" class="alert alert-success fade in fsize16 hide">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Thank You!</strong> Your message successfully sent!
                    </div>
                    <!-- /SENT OK -->

                    <!-- SENT FAILED -->
                    <div id="_sent_required_" class="alert alert-danger fade in fsize16 hide">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Failed!</strong> Please complete all mandatory (*) fields!
                    </div>
                    <!-- /SENT FAILED -->

                    <form id="contactForm" class="white-row" action="php/contact.php" method="post">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label>Full Name *</label>
                                    <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="contact_name" id="contact_name">
                                </div>
                                <div class="col-md-4">
                                    <label>E-mail Address *</label>
                                    <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="contact_email" id="contact_email">
                                </div>
                                <div class="col-md-4">
                                    <label>Phone</label>
                                    <input type="text" value="" data-msg-required="Please enter your phone" data-msg-email="Please enter your phone" maxlength="100" class="form-control" name="contact_phone" id="contact_phone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Subject</label>
                                    <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="contact_subject" id="contact_subject">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Message *</label>
                                    <textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="contact_message" id="contact_message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <span class="pull-right">
                                    <!-- captcha -->
                                    <label class="block text-right fsize12">Antispam Code</label>
                                    <img alt="" rel="nofollow,noindex" width="50" height="18" src="php/captcha.php?bgcolor=ffffff&amp;txtcolor=000000">
                                    <input type="text" name="contact_captcha" id="contact_captcha" value="" data-msg-required="Please enter the subject." maxlength="6" style="width: 100px; margin-left: 10px;">
                                </span>

                                <input id="contact_submit" type="submit" value="Send Message" class="btn btn-primary btn-lg" data-loading-text="Loading...">
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /FORM -->


                <!-- INFO -->
                <div class="col-md-4">

                    <h2>Visit Us</h2>

                    <p>
                        <span class="block"><strong><i class="fa fa-user"></i>&nbsp;Mr. Lukman. Zakaria</strong></span>

                    </p>

                    <div class="divider half-margins">
                        <!-- divider -->
                        <i class="fa fa-star"></i>
                    </div>

                    <p>
                        <span class="block"><strong><i class="fa fa-map-marker"></i>&nbsp;Address:</strong> 2nd Floor, Vishnu Complex, Juni Mangalwari, Opp. Rahate Hospital, CA Road, Nagpur, Maharashtra, India – 440008</span><br />
                        <span class="block"><strong><i class="fa fa-mobile"></i>&nbsp;Mobile:</strong> +91-9970106441</span><br />
                        <span class="block"><strong><i class="fa fa-phone"></i>&nbsp;Phone:</strong> +91-712-6627844</span><br />
                        <span class="block"><strong><i class="fa fa-fax"></i>&nbsp;Fax:</strong> +91-712-2766520</span><br />
                        <span class="block"><strong><i class="fa fa-envelope"></i>&nbsp;Email:</strong> <a href="mailto:lukman@punjaniexports.com">lukman@punjaniexports.com</a> </span><br />
                        <span class="block"><strong><i class="fa fa-skype"></i>&nbsp;Skype ID:</strong> YZPUNJANI</span><br />
                       <%-- <span class="block"><strong><i class="fa fa-google-plus"></i>&nbsp;GTalk ID:</strong> famousinternational</span><br />--%>
                        <span class="block"><strong><i class="fa fa-envelope-o"></i>&nbsp;Alternate Email ID:</strong> info@punjaniexports.com</span><br />
                    </p>

                    <div class="divider half-margins">
                        <!-- divider -->
                        <i class="fa fa-star"></i>
                    </div>

                    <h4 class="font300">Business Hours</h4>
                    <p>
                        <span class="block"><strong>Monday - Friday:</strong> 10am to 6pm</span>
                        <span class="block"><strong>Saturday:</strong> 10am to 2pm</span>
                        <span class="block"><strong>Sunday:</strong> Closed</span>
                    </p>

                </div>
                <!-- /INFO -->

            </div>

        </section>

    </div>
</asp:Content>

