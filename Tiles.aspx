﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Tiles.aspx.cs" Inherits="Tiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="assets/plugins/jquery-2.1.4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var nid = GetParameterValues('id');
            //alert("Hello " + name + " your ID is " + id);

            function GetParameterValues(param) {
                var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < url.length; i++) {
                    var urlparam = url[i].split('=');
                    if (urlparam[0] == param) {
                        return urlparam[1];
                    }
                }
            }

            GetTiles(nid);
        });
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="Scripts/Tiles.js"></script>
    <div id="wrapper" style="padding-top: 111px;">

			<div id="shop">
                <br /><br />
				<!-- PAGE TITLE -->
				

				<section class="container">

					<div class="row">

						

							
						
							<div class="row" id="Products">

								<!-- items -->
								<%--<div class="col-md-3">
							<div class="item-box">
								<figure>
									<a class="item-hover lightbox" href="assets/images/Product4.jpg" data-plugin-options='{"type":"image"}'>
										<span class="overlay color2"></span>
										<span class="inner">
											<span class="block fa fa-plus fsize20"></span>
											<strong>Product Details</strong> DETAIL
										</span>
									</a>
									<img class="img-responsive" src="assets/images/Product4.jpg" width="350" height="260" alt="">
								</figure>
								<div class="item-box-desc">
									<h4>Concept 1</h4>
									
								</div>
							</div>
						</div>--%>
                               
								<!-- items -->

							</div>

							<!-- PAGINATION -->
							
							<!-- /PAGINATION -->

						

						<aside class="col-md-3">

							<h3>Available Series</h3><!-- h3 - have no margin-top -->
							<ul class="nav nav-list" id="SideMenu">
								
							</ul><br />
                            <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                            <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>
							<!-- SHOP FILTERS -->
							
							<!-- /SHOP FILTERS -->

						</aside>

					</div><!-- /row -->

				</section>

			</div>
		</div>
</asp:Content>

