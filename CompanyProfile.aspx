﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CompanyProfile.aspx.cs" Inherits="CompanyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">

        <!-- PAGE TITLE -->
        <%--<header id="page-title">
            <div class="container">
                <h1>Company  <strong>Profile</strong></h1>

                <ul class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Company Profile</li>
                </ul>
            </div>
        </header>--%>
        <header id="page-title" style="background-image:url(images/titleCompany.jpg)">
					<div class="container">
						<h1>Company  <strong>Profile</strong></h1>

                        <ul class="breadcrumb">
							<li><a href="Default.aspx">Home</a></li>
							<li class="active">Company Profile</li>
						</ul><br><br><br><br>
					</div>
				</header>

        <section class="container">

            <!-- WELCOME -->
            <article>
                <h2>Welcome to Punjani Exports</h2>
                <p>Punjani Exports is a reputed name in the Import/Export industry, established in 2002 by Zakaria Family in a small way, by the grace of Almighty and support of all our loyal customers, we have grown exponentially over the past 11 years, known as one of the most reputable Merchant Exporter in India. Based in the city of Nagpur, Maharashtra, the company has kept proving its mettle in the market, through the consistent development and generation of methods and ways to impress and satisfy clients through its Products, Quality, Service and Pricing. The range of products made available by the company is appreciated for the high quality in standards it belongs to.</p>
               
            </article>
            <!-- WELCOME -->

            <div class="divider">
                <!-- divider -->
                <i class="fa fa-star"></i>
            </div>

            <!-- BORN TO BE A WINNER -->
            <article class="row">
                <div class="col-md-6">
						<div class="owl-carousel controlls-over" data-plugin-options='{"items": 1, "singleItem": true, "navigation": true, "pagination": true, "transitionStyle":"fadeUp"}'>
							<div>
								<img class="img-responsive" src="images/About1.jpg" width="555" height="311" alt="">
							</div>
							<div>
								<img class="img-responsive" src="images/About2.jpg" width="555" height="311" alt="">
							</div>
							<div>
								<img class="img-responsive" src="images/About3.jpg" width="555" height="311" alt="">
							</div>
						</div>
					</div>
                <div class="col-md-6">
                    <h3>Punjani Exports : Born to be a Winner</h3>
                    <p>All our efforts are to anticipate our client’s requirements and to exceed their expectations by frequently improving our standard of products and always provide quality service to our clients for a long term relationship. We test our products on various parameters, so as to eliminate the scope of flaws and defects.</p>
                    
                </div>
            </article>
            <!-- /BORN TO BE A WINNER -->


            <div class="divider">
                <!-- divider -->
                <i class="fa fa-star"></i>
            </div>


           
             <!-- FEATURED BOXES 4 -->
            <div class="row featured-box-minimal margin-bottom30">
                <div class="col-md-4">
                    <h4><i class="fa fa-group"></i>Quality Assurance</h4>
                    <p>Our company considers quality to be a significant factor in the product range made available by it. We make sure of high quality standards in all our products through the use of high quality raw materials, on a primary level. </p>
                </div>
                <div class="col-md-4">
                    <h4><i class="fa fa-book"></i>Team</h4>
                    <p>Our well-managed logistics system enables us to ensure lesser turnaround time and complete safety in order delivery. The operations are handled by a team of experienced agents that works with the sole aim of client satisfaction and client delight.</p>
                </div>
                <div class="col-md-4">
                    <h4><i class="fa fa-trophy"></i>Market Covered</h4>
                    <p>We are open to work with any country in the world… At present our favoured countries are: MOZAMBIQUE, ZAMBIA, ANGOLA, NAMIBIA, IVORY COAST, GUNIEA CONAKRY, DJIBOUTI, ROMANIA, GEORGIA & GCC COUNTRIES</p>
                </div>
            </div>
            <!-- /FEATURED BOXES 4 -->

            <!-- FEATURED BOXES 4 -->
            <div class="row featured-box-minimal margin-bottom30">
                <div class="col-md-4">
                    <h4><i class="fa fa-group"></i>Sourcing</h4>
                    <p>The importance of a strong supplier base for success is critical. Over the years, we have devoted great efforts to build alliances with some of the most reliable manufacturers in multiple industries, both in India as well as in China. </p>
                </div>
                <div class="col-md-4">
                    <h4><i class="fa fa-book"></i>efforts </h4>
                    <p>All our efforts are to anticipate our client’s requirements and to exceed their expectations by frequently improving our standard of products and always provide quality service to our clients for a long term relationship.</p>
                </div>
                <div class="col-md-4">
                            </div>
            </div>
            <!-- /FEATURED BOXES 4 -->

            <!-- CALLOUT -->
            <div class="bs-callout text-center nomargin-bottom">
                <h3><strong>let's work together!</strong><a href="Contact.aspx" class="btn btn-primary btn-lg">Contact Me!</a></h3>
            </div>

           <%-- <div class="divider">
               
                <i class="fa fa-star"></i>
            </div>--%>
        </section>

    </div>
</asp:Content>

