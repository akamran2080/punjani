﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Digitalfloo.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Products</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/333.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/333.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 341</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/341.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/341.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/342.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/342.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/412.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/412.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 412</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="SPblack.aspx"><i class="fa fa-circle-o"></i>SP Black Luster Orindary Series</a></li>
                            <li><a href="woodenordinary.aspx"><i class="fa fa-circle-o"></i>Wooden Ordinary and Luster Series</a></li>
                            <li><a href="RedBrown.aspx"><i class="fa fa-circle-o"></i>Red Brown Luster Ordinary Series</a></li>
                            <li><a href="Ivoryluster.aspx"><i class="fa fa-circle-o"></i>Ivory Luster Ordinary Series</a></li>
                            <li><a href="WhiteLuster.aspx"><i class="fa fa-circle-o"></i>White Luster Ordinary Series</a></li>
                            <li><a href="Digitalfloo.aspx"><i class="fa fa-circle-o"></i>Digital Floor GVT</a></li>
                            <li><a href="ElevationLuster.aspx"><i class="fa fa-circle-o"></i>Elevation Luster Ordinary Series</a></li>
                         
                        </ul>
                       <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/333.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/333.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 341</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/341.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/341.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/342.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/342.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/412.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/412.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 412</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/333.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/333.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 341</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/341.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/341.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/342.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/342.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 342</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/412.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/412.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : 412</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    

                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

