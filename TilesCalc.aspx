﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TilesCalc.aspx.cs" Inherits="TilesCalc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="wrapper">

			<div id="shop">

				<!-- PAGE TITLE -->
				<header id="page-title">
					<div class="container">
						<h1>Tiles Calculator</h1>

						<ul class="breadcrumb">
							<li><a href="Default.aspx">Home</a></li>
							<li class="active">Tiles Calculator</li>
						</ul>
					</div>
				</header>

				<section class="container">

					<div class="row">

						<div class="col-md-9">

							<div class="col-md-4">
                                <div class="form-group">
                                    <label for="rw">Room Width&nbsp;(ft)&nbsp;:</label>
                                    <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-resize-horizontal"></i></span>
                                    <input type="number" class="form-control" id="rw" name="rw" placeholder="Room Width">
                                    </div>
                                </div>
                            </div>

						<div class="col-md-4">
                                <div class="form-group">
                                    <label for="rl">Room Length&nbsp;(ft)&nbsp;:</label>
                                    <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-resize-vertical"></i></span>
                                    <input type="number" class="form-control" id="rl" name="rl" placeholder="Room Length">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="mm">Tiles Size&nbsp;(mm)&nbsp;:</label>
                                                                        <select class="cmb form-control" name="mm" type="option">
                                        <option value="0">Select Size</option>
                                                                                <option value="20 X 30cm">20 X 30cm</option>
                                                                                <option value="25 X 32.5cm">25 X 32.5cm</option>
                                                                                <option value="25  X  50cm">25  X  50cm</option>
                                                                                <option value="30 X 30cm">30 X 30cm</option>
                                                                                <option value="30 X 45cm">30 X 45cm</option>
                                                                                <option value="30 X 60cm">30 X 60cm</option>
                                                                                <option value="30 X 90cm">30 X 90cm</option>
                                                                                <option value="39.6 X 39.6cm">39.6 X 39.6cm</option>
                                                                                <option value="45 X 45cm">45 X 45cm</option>
                                                                                <option value="60 X 60cm">60 X 60cm</option>
                                                                                <option value="60 X 120cm">60 X 120cm</option>
                                                                                <option value="80 X 80cm">80 X 80cm</option>
                                                                                <option value="25 X 37.5cm">25 X 37.5cm</option>
                                                                                <option value="40 X 40cm">40 X 40cm</option>
                                                                                
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="button" onclick="doCalc();" class="btn btn-primary btn-lg" name="btnCalc"><i class="fa fa-check"></i> Calculate</button>
                                <!--<input type="button" onclick="doCalc();" class="btn btn-primary btn-lg" value="Calculate" name="btnCalc">-->
                                <button type="reset" class="btn btn-danger btn-lg" name="btnClear"><i class="fa fa-repeat"></i> Clear</button>
                                <!--<input type="reset" class="btn btn-warning btn-lg" value="Clear" name="btnClear">-->
                            </div>
                             <div class="col-md-4">
                                <div class="form-group"><br />
                                    <label for="tn">Total Tiles Need (Approx.):</label>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon "><i class="icon-th"></i></span>
                                        <input type="text" class="form-control" value="" name="tn" disabled="">
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group"><br />
                                    <label for="tn">Total Tiles Area (ft) :</label>
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon"><i class="icon-dropbox"></i></span>
                                        <input type="text" class="form-control" value="" name="ta" disabled="">
                                    </div> 
                                </div>
                            </div>
                           
                           
							<div class="row">
                                
								

							</div>

							<!-- PAGINATION -->
							<div class="row">

								

							</div>
							<!-- /PAGINATION -->

						</div><!-- /col-md-9 -->

						<aside class="col-md-3">

							<h3>Product Categories</h3><!-- h3 - have no margin-top -->
							<ul class="nav nav-list">
								<li><a href="#"><i class="fa fa-circle-o"></i> Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Floor Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Porcelain Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor Tiles</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor GVT</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Sanitarywares</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Others</a></li>
							</ul><br />
                            <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                            <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>
							<!-- SHOP FILTERS -->
							
							<!-- /SHOP FILTERS -->

						</aside>

					</div><!-- /row -->

				</section>

			</div>
		</div>
</asp:Content>

