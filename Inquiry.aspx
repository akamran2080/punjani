﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Inquiry.aspx.cs" Inherits="Inquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="wrapper" style="padding-top: 90px;">

			<div id="shop">

				<!-- PAGE TITLE -->
				<header id="page-title">
					<div class="container">
						<h1></h1>

						
					</div>
				</header>
                <header id="page-title" style="background-image:url(images/titleInquiary.jpg)">
					<div class="container">
						<h1 style="color:green"><strong>I</strong>nquiry</h1>
							<ul class="breadcrumb">
							<li><a href="Default.aspx">Home</a></li>
							<li class="active">Inquiry</li>
						</ul>
						</ul><br><br><br><br>
					</div>
				</header>

				<section class="container">

					<div class="row">
						<div class="col-md-8">
						
							<h2>Inquiry</h2>
							<form class="white-row" method="post" action="shop-cc-pay.html">
								

								

								<!-- BILLING ADDRESS -->
								<div class="row">
									<%--<div class="form-group">
										<div class="col-md-12">
											<label>Country</label>
											<select class="form-control pointer">
												<option value="">Select a country</option>
											</select>
										</div>
									</div>--%>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>*Contact Person</label>
											<input type="text" value="" class="form-control">
										</div>
										<div class="col-md-6">
											<label>Company Name </label>
											<input type="text" value="" class="form-control">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Address </label>
											<input type="text" value="" class="form-control">
										</div>
									</div>
								</div>
                                <div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>*Email</label>
											<input type="text" value="" class="form-control"/>
										</div>
										<div class="col-md-6">
											<label>Phone </label>
											<input type="text" value="" class="form-control"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>*City</label>
											<input type="text" value="" class="form-control"/>
										</div>
										<div class="col-md-6">
											<label>State </label>
											<input type="text" value="" class="form-control"/>
										</div>
									</div>
								</div>
                                <div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>*Country</label>
											<input type="text" value="" class="form-control"/>
										</div>
										<div class="col-md-6">
											<label>Postal Code </label>
											<input type="text" value="" class="form-control"/>
										</div>
									</div>
								</div>
                                <div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>Fax</label>
											<input type="text" value="" class="form-control"/>
										</div>
										<div class="col-md-6">
											<label>*Enquiry For</label>
											<input type="text" value="" class="form-control"/>
										</div>
									</div>
								</div>
								<!-- /BILLING ADDRESS -->


								
								<!-- /SHIPPING ADDRESS -->

								<!-- PAYMNET SELECTOR -->
								
								<!-- /PAYMNET SELECTOR -->

								<!-- AGREE -->
								<%--<div class="row">
									<div class="col-md-12">
										<label class="pointer shop-cart-agree">
											<input type="checkbox" name="agree" value="1"> I understand and agree the <a href="page-terms-and-conditions.html" target="_blank">terms and conditions</a>.
										</label>
									</div>
								</div>--%>
								<!-- /AGREE -->

								<button class="btn btn-primary btn-lg">Send</button>
							</form>

						</div>

                        <aside class="col-md-3">

							<h3>CATEGORIES</h3><!-- h3 - have no margin-top -->
							<ul class="nav nav-list">
								<li><a href="#"><i class="fa fa-circle-o"></i> Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Floor Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Porcelain Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor Tiles</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor GVT</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Sanitarywares</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Others</a></li>
							</ul><br />
                            <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                            <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>

							<!-- SHOP FILTERS -->
							
							<!-- /SHOP FILTERS -->

						</aside>
						
					</div>

				</section>

			</div>
		</div>
</asp:Content>

