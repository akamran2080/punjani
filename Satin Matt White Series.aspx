﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Satin Matt White Series.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Satin Matt White Series</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC10064.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC10064.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :SC10064</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-173.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-173.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-173</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-178.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-178.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-178</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-179.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-179.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-179</h4>

                            </div>
                        </div>
                    </div> 
                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Vitro Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy White Series</a></li>
                            <li><a href="Satin Matt White Series.aspx"><i class="fa fa-circle-o"></i>Satin Matt White Series</a></li>
                            <li><a href="Plain Cloudy Series.aspx"><i class="fa fa-circle-o"></i>Plain Cloudy Series</a></li>
                            <li><a href="Glossy Wooden Series.aspx"><i class="fa fa-circle-o"></i>Glossy Wooden Series</a></li>
                            <li><a href="Glossy Black Series.aspx"><i class="fa fa-circle-o"></i>Glossy Black Series</a></li>
                            <li><a href="Vitro Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy Ivory Series</a></li>
                            <li><a href="Satin Matt Ivory Series.aspx"><i class="fa fa-circle-o"></i>Satin Matt Ivory Series</a></li>
                            <li><a href="Matt White Ivory Series.aspx"><i class="fa fa-circle-o"></i>Matt White Ivory Series</a></li>
                            <li><a href="Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Glossy White Series</a></li>
                            <li><a href="Glossy Bhama Series.aspx"><i class="fa fa-circle-o"></i>Glossy Bhama Series</a></li>
                            <li><a href="Sp Satin Matt and Wooden Series.aspx"><i class="fa fa-circle-o"></i>Sp Satin Matt and Wooden Series</a></li>
                            <li><a href="Rustic Series.aspx"><i class="fa fa-circle-o"></i>Rustic Series</a></li>
                            <li><a href="Leather Series.aspx"><i class="fa fa-circle-o"></i>Leather Series</a></li>
                            <li><a href="Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Glossy Ivory Series</a></li>
                            <li><a href="Galaxy Brown Series.aspx"><i class="fa fa-circle-o"></i>Galaxy Brown Series</a></li>
                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-351.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-351.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-351</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-352.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-352.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :   SCVT-352</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/ SCVT-353.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-353.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-353</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-354.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-354.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-354</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                     <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-355.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-355.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-355</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-356.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-356.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-356</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-357.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-357.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :   SCVT-357</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/ SCVT-358.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-358.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-358</h4>

                            </div>
                        </div>
                    </div>
                   
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-359.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-359.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-359</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-360.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-360.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-360</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-361.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-361.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-361</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SCVT-362.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SCVT-362.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SCVT-362</h4>

                            </div>
                        </div>
                    </div>
                                      
                    <!-- items -->
                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->

                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

