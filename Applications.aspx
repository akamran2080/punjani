﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Applications.aspx.cs" Inherits="Applications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="wrapper">

			<div id="shop">

				<!-- PAGE TITLE -->
				
                <header id="page-title" style="background-image:url(images/titleApp.jpg)">
					<div class="container">
						<h1><strong>A</strong>pplications</h1>

                       <ul class="breadcrumb">
							<li><a href="Default.aspx">Home</a></li>
							<li class="active">Applications</li>
						</ul><br><br><br><br>
					</div>
				</header>

				<section class="container">

					<div class="row">

						<div class="col-md-9">

							

						
							<div class="row">

								<!-- items -->
								
								<!-- items -->

							</div>

							<!-- PAGINATION -->
							<div class="row">

								

							</div>
							<!-- /PAGINATION -->

						</div><!-- /col-md-9 -->

						<aside class="col-md-3">

							
							<h3>Product Categories</h3><!-- h3 - have no margin-top -->
							<ul class="nav nav-list">
								<li><a href="#"><i class="fa fa-circle-o"></i> Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Floor Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Porcelain Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Wall Tiles</a></li>
								<li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor Tiles</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Digital Floor GVT</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Sanitarywares</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Others</a></li>
							</ul><br />
                            <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                            <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>
							<!-- SHOP FILTERS -->
							<form method="get" action="#">

								<!-- price slider -->
								
								<script type="text/javascript" charset="utf-8">
									var slider_config = { from: 10, to: 500, heterogeneity: ['50/100', '75/250'], step: 10, dimension: '&nbsp;$', skin: 'round_plastic' };
								</script>
								<!-- /price slider -->

								

								<hr class="half-margins" />

								
							</form>
							<!-- /SHOP FILTERS -->

						</aside>

					</div><!-- /row -->

				</section>

			</div>
		</div>
</asp:Content>

