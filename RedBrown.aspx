﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RedBrown.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx"></a></li>
                        <h1>Red Brown Luster Ordinary Series</h1>
                        <li class="active"></li>
                    </ul>
                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/7001.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/7001.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 7001</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/7003.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/7003.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 7003</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/7004.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/7004.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 7004</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/7005.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/7005.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 7005</h5>

                            </div>
                        </div>
                    </div>
                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="SPblack.aspx"><i class="fa fa-circle-o"></i>SP Black Luster Orindary Series</a></li>
                            <li><a href="woodenordinary.aspx"><i class="fa fa-circle-o"></i>Wooden Ordinary and Luster Series</a></li>
                            <li><a href="RedBrown.aspx"><i class="fa fa-circle-o"></i>Red Brown Luster Ordinary Series</a></li>
                            <li><a href="Ivoryluster.aspx"><i class="fa fa-circle-o"></i>Ivory Luster Ordinary Series</a></li>
                            <li><a href="WhiteLuster.aspx"><i class="fa fa-circle-o"></i>White Luster Ordinary Series</a></li>
                            <li><a href="ElevationLuster.aspx"><i class="fa fa-circle-o"></i>Elevation Luster Ordinary Series</a></li>
                         
                        </ul>
                       <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/7006.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/7006.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 7006</h5>

                            </div>
                        </div>
                    </div>
                   
                    <!-- items -->
                   
                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    

                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

