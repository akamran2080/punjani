﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Rustic Series.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Products</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45001.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45001.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45001</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45002.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45002.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45002</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45030.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45030.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45030</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45031.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45031.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45031</h4>

                            </div>
                        </div>
                    </div>
                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Vitro Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy White Series</a></li>
                            <li><a href="Satin Matt White Series.aspx"><i class="fa fa-circle-o"></i>Satin Matt White Series</a></li>
                            <li><a href="Plain Cloudy Series.aspx"><i class="fa fa-circle-o"></i>Plain Cloudy Series</a></li>
                            <li><a href="Glossy Wooden Series.aspx"><i class="fa fa-circle-o"></i>Glossy Wooden Series</a></li>
                            <li><a href="Glossy Black Series.aspx"><i class="fa fa-circle-o"></i>Glossy Black Series</a></li>
                            <li><a href="Vitro Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Vitro Glossy Ivory Series</a></li>
                            <li><a href="Satin Matt Ivory Series.aspx"><i class="fa fa-circle-o"></i>Satin Matt Ivory Series</a></li>
                            <li><a href="Matt White Ivory Series.aspx"><i class="fa fa-circle-o"></i>Matt White Ivory Series</a></li>
                            <li><a href="Glossy White Series.aspx"><i class="fa fa-circle-o"></i>Glossy White Series</a></li>
                            <li><a href="Glossy Bhama Series.aspx"><i class="fa fa-circle-o"></i>Glossy Bhama Series</a></li>
                            <li><a href="Sp Satin Matt and Wooden Series.aspx"><i class="fa fa-circle-o"></i>Sp Satin Matt and Wooden Series</a></li>
                            <li><a href="Rustic Series.aspx"><i class="fa fa-circle-o"></i>Rustic Series</a></li>
                            <li><a href="Leather Series.aspx"><i class="fa fa-circle-o"></i>Leather Series</a></li>
                            <li><a href="Glossy Ivory Series.aspx"><i class="fa fa-circle-o"></i>Glossy Ivory Series</a></li>
                            <li><a href="Galaxy Brown Series.aspx"><i class="fa fa-circle-o"></i>Galaxy Brown Series</a></li>
                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45032.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45032.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45032</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45051.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45051.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45051</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45054.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45054.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45054</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45055.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45055.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45055</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45057.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45057.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45057</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45058.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45058.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45058</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45059.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45059.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45059</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45060.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45060.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45060</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45061.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45061.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45061</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45062.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45062.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45062</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC45063.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC45063.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC45063</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="400x400/SC46001.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Floor</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="400x400/SC46001.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SC46001</h4>

                            </div>
                        </div>
                    </div>

                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->

                </div>
                <!-- /row -->


            </section>

        </div>
</asp:Content>

