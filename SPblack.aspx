﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SPblack.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">

                   <ul class="breadcrumb">
                        <li><a href="Default.aspx"></a></li>
                        <h1>SP Black Luster Orindary Series</h1>
                        <li class="active"></li>
                    </ul>

                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8801.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8801.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8801</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8802.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8802.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8802</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8803.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8803.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8803</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8804.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8804.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8804</h5>

                            </div>
                        </div>
                    </div>
                    <!-- items -->



                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="SPblack.aspx"><i class="fa fa-circle-o"></i>SP Black Luster Orindary Series</a></li>
                            <li><a href="woodenordinary.aspx"><i class="fa fa-circle-o"></i>Wooden Ordinary and Luster Series</a></li>
                            <li><a href="RedBrown.aspx"><i class="fa fa-circle-o"></i>Red Brown Luster Ordinary Series</a></li>
                            <li><a href="Ivoryluster.aspx"><i class="fa fa-circle-o"></i>Ivory Luster Ordinary Series</a></li>
                            <li><a href="WhiteLuster.aspx"><i class="fa fa-circle-o"></i>White Luster Ordinary Series</a></li>
                            <li><a href="ElevationLuster.aspx"><i class="fa fa-circle-o"></i>Elevation Luster Ordinary Series</a></li>
                         
                        </ul>
                       <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8805.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8805.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8805</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8806.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8806.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8806</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8807.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8807.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8807</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8808.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8808.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8808</h5>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8809.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8809.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8809</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8810.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8810.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8810</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8811.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8811.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8811</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8812.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8812.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8812</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8813.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8813.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8813</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8814.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8814.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8814</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8815.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8815.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8815</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8816.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8816.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8816</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8817.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8817.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8817</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8818.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8818.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8818</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8819.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8819.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8819</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8820.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8820.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8820</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8821.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8821.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8821</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8822.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8822.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8822</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8823.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8823.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8823</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8824.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8824.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8824</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8825.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8825.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8825</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8826.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8826.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8826</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/8827.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/8827.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 8827</h5>

                            </div>
                        </div>
                    </div>

v
                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    

                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

