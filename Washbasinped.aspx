﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Washbasinped.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    <h1>Wash Basin With Pedestal</h1>

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">Products</li>
                    </ul>
                </div>
            </header>

            <section class="container">







                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-CLASSIC-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-CLASSIC-WB-WITH-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-CLASSIC-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-EURO-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong>
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-EURO-WB-WITH-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-EURO-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-FABIA-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-FABIA-WB-WITH-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-FABIA-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-GLORY-30X18-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong>
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-GLORY-30X18-WB-WITH-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-GLORY-30X18-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>

                    <!-- items -->

                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->

                    <aside class="col-md-3">

                        <h3>Sanitarywares</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="Stone.aspx"><i class="fa fa-circle-o"></i>Accessories</a></li>
                            <li><a href="Spcrystal.aspx"><i class="fa fa-circle-o"></i>Antique Craft Basin Wih Pedestal</a></li>
                            <li><a href="Maxcio.aspx"><i class="fa fa-circle-o"></i>Antique Craft Couple Suite</a></li>
                            <li><a href="Hyper.aspx"><i class="fa fa-circle-o"></i>Antique Craft Table Top</a></li>
                            <li><a href="GlassIvory.aspx"><i class="fa fa-circle-o"></i>Antique Craft Wall Mounted</a></li>
                            <li><a href="Glossybhama.aspx"><i class="fa fa-circle-o"></i>Counter Basin</a></li>
                            <li><a href="Coin.aspx"><i class="fa fa-circle-o"></i>Couple Suite</a></li>
                            <li><a href="Arc.aspx"><i class="fa fa-circle-o"></i>Couplue Suites White</a></li>
                            <li><a href="SPlaincolour.aspx"><i class="fa fa-circle-o"></i>EWC S Vitrossa</a></li>
                            <li><a href="Sspcoin.aspx"><i class="fa fa-circle-o"></i>One Piece</a></li>
                            <li><a href="Plaincolour.aspx"><i class="fa fa-circle-o"></i>Pan and Urinals</a></li>
                            <li><a href="Mattwhite.aspx"><i class="fa fa-circle-o"></i>Plain Suites</a></li>
                            <li><a href="Glossywooden.aspx"><i class="fa fa-circle-o"></i>Printed Collection</a></li>
                            <li><a href="GlossyBrown.aspx"><i class="fa fa-circle-o"></i>Stargold Vitrosa Collection</a></li>
                            <li><a href="Checkres.aspx"><i class="fa fa-circle-o"></i>Table Top</a></li>
                            <li><a href="Aqua.aspx"><i class="fa fa-circle-o"></i>Wall Hung</a></li>
                            <li><a href="Rustic.aspx"><i class="fa fa-circle-o"></i>Wall Mounted</a></li>
                            <li><a href="Washbasin.aspx"><i class="fa fa-circle-o"></i>Wash Basin</a></li>
                            <li><a href="Washrani.aspx"><i class="fa fa-circle-o"></i>Wash Basin RANI</a></li>
                            <li><a href="Washbasinped.aspx"><i class="fa fa-circle-o"></i>Wash Basin With Pedestal</a></li>
                            <li><a href="Watercloset.aspx"><i class="fa fa-circle-o"></i>Water Closet</a></li>

                        </ul>
                        <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->

                    </aside>

                </div>
                <div class="row">

                    <!-- items -->

                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-LAURET-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-LAURET-WB-WITH-PEDESTAL.jpg"  alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-LAURET-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-LOTUS-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-LOTUS-WB-WITH-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-LOTUS-WB-WITH-PEDESTAL-</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-MATIZ-22X-16-WB-SET.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-MATIZ-22X-16-WB-SET.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-MATIZ-22X-16-WB-SET</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-NORMUS-WB-WITH-HALF-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-NORMUS-WB-WITH-HALF-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-NORMUS-WB-WITH-HALF-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                  <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-REPOSE-WB-WITH-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-REPOSE-WB-WITH-PEDESTAL.jpg"  alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-REPOSE-WB-WITH-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-YUGA-WB-WITH-HALF-PEDESTAL.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-YUGA-WB-WITH-HALF-PEDESTAL.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-YUGA-WB-WITH-HALF-PEDESTAL</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Big-Sterling-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Big-Sterling-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Big-Sterling-Set</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Capri-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Capri-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Capri-Set</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Repose-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Repose-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Repose-Set</h4>

                            </div>
                        </div>
                    </div>
                     <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Repose-Set-500-X-400.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Repose-Set-500-X-400.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Repose-Set-500-X-400</h4>

                            </div>
                        </div>
                    </div>
                     <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Sonata-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Sonata-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Sonata-Set</h4>

                            </div>
                        </div>
                    </div>
                   <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Stargold-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Stargold-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Stargold-Set</h4>

                            </div>
                        </div>
                    </div>
                  <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Sterling-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Sterling-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Sterling-Set</h4>

                            </div>
                        </div>
                    </div>
                     <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Suprme-Set.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Suprme-Set.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Suprme-Set</h4>

                            </div>
                        </div>
                    </div>
                     <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Wooden.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Wooden.jpg"  alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID : SL-Wooden</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="sanitary/pedest/SL-Wooden-A.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Sanitarywares</strong> 
                                    </span>
                                </a>
                                <img class="img-responsive" src="sanitary/pedest/SL-Wooden-A.jpg"  alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h4>Product ID :  SL-Wooden-A</h4>

                            </div>
                        </div>
                    </div>

                  
                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->





                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

