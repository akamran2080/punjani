﻿function userLogin() {
    debugger;
    var sUserNames = $("#Username").val();
    var sPasswords = $("#Password").val();
    if (sUserNames == "") {
        alert("Please enter user name.");
        $("#Username").focus();
    }
    else if (sPasswords == "") {
        alert("Please enter password");
        $("#Password").focus();
    }
    
    else {
        $.ajax({
            type: 'POST',
            url: 'LoginHandler.asmx/UserLogin',
            dataType: 'json',
            data: "{'sUserName':'" + sUserNames + "','sPassword':'" + sPasswords + "'}",
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
               
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        window.location.href = "http://admin.punjaniexports.com/Dashboard.aspx";
                    }
                    else {
                        alert("Invalid username and password.");
                    }
                
            },
            error: function () {
                alert("Login failed.");
                return false;
            }
        });
    }
}