﻿
var ProductName;
var SideMenu;
function Products(nID) {

    window.location.href = "Tiles.aspx?id=" + nID
}
function GetTiles(nID)
{
    debugger;
    var arrnID = new Array();
    $.ajax({
        type: "POST",
        url: "TilesHandler.asmx/Products",
        //data: '{"nCategoryParentID":"' + 0 + '"}',
        //data: '{}',
        data: '{"nID":"' + nID + '"}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#Products").empty();
            var sMainCategory = "";
            var ul = '';
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var sMainCategory = result.tbl_Products;
            var Url;
            var data='{"type":"image"}';

            for (var i = 0; i < sMainCategory.length; i++) {
                Url = "Admin/uploads/" + sMainCategory[i].sProductImage;
                ul += '<div class="col-md-3">'
                ul += '<div class="item-box">'
                ul += '<figure>'
                ul += '<a class="item-hover lightbox" href="' + Url + '" data-plugin-options=' + data + '>'
                ul += '<span class="overlay color2"></span>'
                ul += '<span class="inner">'
                ul += '<span class="block fa fa-plus fsize20"></span>'
                ul += '<strong>Details: ' + sMainCategory[i].sProductName + '</strong>'
                ul += '</span>'
                ul += '</a>'
                ul += '<img class="img-responsive" src="' + Url + '" width="350" height="260" alt="">'
                ul += '</figure>'
                ul += '<div class="item-box-desc">'
                ul += '<h5>Code: ' + sMainCategory[i].sManufacturerNumber + '</h5>'
                //ul += '<h4>Category: ' + sMainCategory[i].sProductName + '</h4>'
                ul += '</div>'
                ul += '</div>'
                ul += '</div>'
            }


            ProductName = ul;
            $("#Products").append(ProductName);
            GetTilesSideMenu(nID);
        },
        error: function () {
        }
    });
}

function GetTilesSideMenu(nID) {
    debugger;
    var arrnID = new Array();
    $.ajax({
        type: "POST",
        url: "TilesHandler.asmx/SideMenu",
        //data: '{"nCategoryParentID":"' + 0 + '"}',
        //data: '{}',
        data: '{"nID":"' + nID + '"}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#SideMenu").empty();
            var sMainCategory = "";
            var ul = '';
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var sMainCategory = result.tbl_Menu;

            for (var i = 0; i < sMainCategory.length; i++) {
                
                ul += '<li><a href="#"><i class="fa fa-circle-o"></i> ' + sMainCategory[i].sMenuItem + '</a></li>'
            }

            SideMenu = ul;
            $("#SideMenu").append(SideMenu);

        },
        error: function () {
        }
    });
}