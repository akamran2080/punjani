﻿<%@ WebHandler Language="C#" Class="UploadHandler2" %>

using System;
using System.Web;
using System.IO;
using System.Collections.Generic;

public class UploadHandler2 : IHttpHandler, System.Web.SessionState.IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context) {
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string myPath = file.FileName;
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;

                    fname = context.Session["RandomNo"].ToString()+"_3.jpg";
                   
                }
                 fname=Path.Combine(context.Server.MapPath("uploads"), fname);
                file.SaveAs(fname);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("File Uploaded Successfully!");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
}