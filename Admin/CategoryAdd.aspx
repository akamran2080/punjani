﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CategoryAdd.aspx.cs" Inherits="Admin_CategoryAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="scripts/CategoryAdd.js" type="text/javascript"></script>
    <table cellpadding="0" cellspacing="0" border="0" width="982px">
        <tr width="982px">
            <td colspan="2"></td>
        </tr>
        <tr style="height: 1px">
            <td colspan="2"></td>
        </tr>
        <tr>
            <td class="admin-content">
                <div class="row1">
                    <div class="h3subheader">
                        Add Category and Sub-Category
                    </div>
                    <div class="row2Homepage">
                        <div class="searchIDHomepage">
                        </div>
                        <div>
                            <table id="CategoriesSelect" cellpadding="0" cellspacing="0" width="50%">
                                <tr id="0">
                                    <td style="vertical-align: top; padding: 10px">
                                        <label class="custom-selectEvent">
                                            <select id="Select_Category" onchange="OnChange_GetSubCategory(0,0);">
                                          <%--  <select id="Select_Category" onchange="OnChange_GetSubCategory(0,0);">--%>
                                                <option value="">Select Category</option>
                                                <%--"Here Category will be displayed from DB--%>
                                            </select>
                                        </label>
                                    </td>
                                    <td style="padding-left: 0px">
                                         <%--<input type="button" id="btnAddCategory" value="Add Category" style="float: left" onclick=" " />--%>
                                        <input type="button" id="btnAddCategory" value="Add Category" style="float: left" onclick="return ShowAddCategoryDialog();" />
                                    </td>
                                    <td style="padding-left: 0px">
                                        <input type="button" id="btnAddSubCategory" value="Add Sub Category" style="float: left" onclick="return ShowAddSubCategoryDialog(0);" />
                                    </td>
                                    <td style="padding-left: 0px">
                                        <input type="button" id="btnDelCategory" value="Delete Category" style="float: left" onclick="return ShowDeleteCategoryDialog(0);" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; padding: 10px">
                                        <label class="custom-selectEvent">
                                            <select id="Select_SubCategory">
                                                
                                            </select>
                                        </label>
                                    </td>
                                    <td style="padding-left: 0px">
                                        <input type="button" id="Button1" value="Add Sub-Category" style="float: left" onclick="return ShowAddSubCategoryDialog();" />
                                    </td>
                                    <td style="padding-left: 0px">
                                        <input type="button" id="Button2" value="Delete Sub-Category" style="float: left" onclick="return ShowDeleteSubCategoryDialog();" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <%--<div class="h3subheader">Products of Selected Category</div>
                <br />
                <div class="admin-tblHeader">
                    <span class="admin-tblHeaderName">Sr. No.</span>
                    <span class="admin-tblEventType">Product ID</span>
                    <span class="admin-tblUserName">Available Quantity</span>
                    <span class="admin-tblExtra">Details</span>
                </div>
                <div class="tblList">
                    <table class="DisplayTableDashboard" id="tblUsersEvents" width="100%" border="0">
                        <tr>
                            <td width="200">1</td>
                            <td width="280">PS908432</td>
                            <td width="290">23</td>
                            <td width="200">View Details</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="tblSeparator"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">2</td>
                            <td width="280">PS395092</td>
                            <td width="290">43</td>
                            <td width="200">View Details</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="tblSeparator"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">3</td>
                            <td width="280">PS091384</td>
                            <td width="290">500</td>
                            <td width="200">View Details</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="tblSeparator"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">3</td>
                            <td width="280">PS098385</td>
                            <td width="290">23</td>
                            <td width="200">View Details</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="tblSeparator"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div style="float: right"><a href="#">View all</a></div>
                            </td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>

                </div>
            </td>
            <td>
                <img src="images/spacer.gif" width="1" height="388" alt="" />
            </td>--%>
        </tr>
    </table>

   <div id="Dialog_AddCategory" style="display: none">
                        <div class="row1subscriptionpopup222">
                            <div class="popupdiv">
                                <table id="tblCategoryImage" class="MainTable" style="margin-right: 20px">
                                    <tr>
                                        <td colspan="2">
                                            Please enter the name and press the submit button
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150" style="padding-left: 10px">
                                            Category Name
                                        </td>
                                        <td width="200">
                                            <input id="text_AddCategory" type="text" name="text_AddCategory" class="TextBox_CategoryTestName" />
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>

   <div id="Dialog_SubAddCategory" style="display: none">
                        <div class="row1subscriptionpopup222">
                            <div class="popupdiv">
                                <table id="Table1" class="MainTable" style="margin-right: 20px">
                                    <tr>
                                        <td colspan="2">
                                            Please enter the name and press the submit button
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150" style="padding-left: 10px">
                                            Category Name
                                        </td>
                                        <td width="200">
                                            <input id="text_SubCategory" type="text" name="text_SubCategory" class="TextBox_CategoryTestName" />
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>

     <div id="Dialog_DeleteCategory" style="display: none">
                        <div class="row1subscriptionpopup222">
                            <div class="popupdiv">
                                <table id="Table1" class="MainTable" style="margin-right: 20px">
                                    <tr>
                                        <td colspan="2">
                                            Are you sure want to delete the category?? 
                                        </td>
                                    </tr>
                                    
                                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>
        <div id="Dialog_DeleteSubCategory" style="display: none">
                        <div class="row1subscriptionpopup222">
                            <div class="popupdiv">
                                <table id="Table1" class="MainTable" style="margin-right: 20px">
                                    <tr>
                                        <td colspan="2">
                                            Are you sure want to delete the Sub-Category?? 
                                        </td>
                                    </tr>
                                    
                                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>
</asp:Content>

