﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/login.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <link href="css/excite-bike/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <script language="javascript"  type="text/javascript">
        $(document).ready(function () {
            $("#adusername").focus();
        });
        function userLogin() {
        //http://www.asp.net/mvc/tutorials/mvc-5/introduction/creating-a-connection-string
            var sUserNames = $("#adusername").val();
            var sPasswords = $("#adpassword").val();
            if (sUserNames == "") {
                alert("Please enter user name.");
                $("#adusername").focus();
            }
            else if (sPasswords == "") {
                alert("Please enter password");
                $("#adpassword").focus();
            }
            else if (sPasswords.length < 6) {
                alert("Paswword should be atleast 6 character long");
                $("#adpassword").focus();
                
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: 'AdminHandler.asmx/UserLogin',
                    dataType: 'json',
                    data: "{'sUserName':'" + sUserNames + "','sPassword':'" + sPasswords + "'}",
                    contentType: 'application/json; charset=utf-8',                   
                    success: function (data) {
                        try {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            if (result.retCode == 1) {
                                window.location.href = "Dashboard.aspx";
                            }
                            else {
                                alert("Invalid username and password.");
                            }
                        }
                        catch (e) { }
                    },
                    error: function () {
                        alert("Login failed.");
                        return false;
                    }
                });
            }
        }
        function LoginUserKeyPress(keyCode) {
            if (keyCode == 13) {
                userLogin();
            }
        }
    $(function() {
        $("#button").button();
    });
    </script>


</head>
<body>
    <form id="form1" runat="server" class="nice" onkeypress="return LoginUserKeyPress(event.keyCode);">
        <div class="row">
            <div class="eight columns centered">
                <div class="login_box">
                    <div class="lb_content">
                        <div class="login_logo">
                            <img src="images/logo.png" alt="logo" />
                        </div>
                        <h2 class="lb_ribbon lb_blue"><span>Admin Login area</span></h2>
                        <a href="#" class="right small sl_link"><span>Forgot your password?</span> </a>
                        <div class="row m_cont">
                            <div class="eight columns centered">
                                <div class="l_pane">
                                    <div class="sepH_c">
                                        <label for="username">Username</label>
                                        <input type="text" id="adusername" value="" name="adusername" class="oversize expand input-text" />
                                        <label for="password">Password</label>
                                        <input type="password" id="adpassword" value="" name="adpassword" class="oversize expand input-text" />
                                    </div>
                                    <label for="remember" class="left">
                                        <input type="checkbox" id="remember" />
                                        Remember me</label>
                                    <input type="button" style="float: right" id="button" onclick="return userLogin();" value="Login" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
