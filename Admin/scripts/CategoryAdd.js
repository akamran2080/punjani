﻿$(document).ready(function () {
    //fumction to load main ie parent 0 categories
    showMainCategory();
    $("#Dialog_AddCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Category",
        modal: true,
        resizable: false,
        width: '500px',
        buttons: [
            {
                text: "Submit",
                click: function () {
                    AddCategory();                    
                }
            },
            {
                text: "Cancel",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
    $("#Dialog_SubAddCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Sub-Category",
        modal: true,
        resizable: false,
        width: '500px',
        buttons: [
            {
                text: "Submit",
                click: function () {
                    AddSubCategory(sSubParentTrID);
                }
            },
            {
                text: "Cancel",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
    
    $("#Dialog_DeleteCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Delete Category",
        modal: true,
        resizable: false,
        width: '500px',
        buttons: [
            {
                text: "Yes",
                click: function () {
                    deleteCategory(CatToDelete, GetSubCat);
                }
            },
            {
                text: "NO",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

    $("#Dialog_DeleteSubCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Delete Sub-Category",
        modal: true,
        resizable: false,
        width: '500px',
        buttons: [
            {
                text: "Yes",
                click: function () {
                }
            },
            {
                text: "NO",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
});

var sSubParentTrID;
var sDelParentId;
var CatToDelete;
var GetSubCat;
function ShowAddCategoryDialog() {
    $("#Dialog_AddCategory").dialog("open");
    event.preventDefault();
}
function ShowAddSubCategoryDialog(trID) {
    sSubParentTrID = trID;
    $("#Dialog_SubAddCategory").dialog("open");
    event.preventDefault();
}
function ShowDeleteCategoryDialog(idval,trID) { //ShowDeleteSubCategoryDialog
    CatToDelete = idval;
    GetSubCat = trID;
    $("#Dialog_DeleteCategory").dialog("open");
    event.preventDefault();
}
function ShowDeleteSubCategoryDialog(trID) {
    sDelParentId = trID;
    $("#Dialog_DeleteSubCategory").dialog("open");
    event.preventDefault();
}
function AddCategory() {
    var category = $("#text_AddCategory").val();
    if (category == "") {
        alert("Please enter category name.");
    }
    else {
        $.ajax({
            type: "POST",
            url: "AdminHandler.asmx/Add_Category",
            data: '{"sCategory":"' + category + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Default.aspx"; // Session end
                        return false;
                    }
                    if (result.retCode == 1) {
                        alert("Category added successfully");
                        $("#Dialog_AddCategory").dialog("close");
                        showMainCategory();
                    }
                    else {
                        alert("Cannot add category");
                        $("#Dialog_AddCategory").dialog("close");
                    }
                }
                catch (e) { }

            },
            error: function () {
                alert("Adding category failed");
                $("#Dialog_AddCategory").dialog("close");
            }
        });
    }
}
function AddSubCategory(trID) {
    var sSubCategory = $("#text_SubCategory").val();
    if (trID == 0) {
        var sParentCategory = $("select#Select_Category option:selected").val();
        //var tblRow = 
    }
    else {
        var sParentCategory = $("select#SelectFirstLevelCategories" + trID + " option:selected").val();
    }
    if (sSubCategory == "") {
        alert("Please enter Subcategory name.");
    }
    else {
        $.ajax({
            type: "POST",
            url: "AdminHandler.asmx/Add_SubCategory",
            data: '{"subCategory":"' + sSubCategory + '","mainCategory":"' + sParentCategory + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Default.aspx"; // Session end
                        return false;
                    }
                    if (result.retCode == 1) {
                        alert("Sub Category added successfully");
                        $("#Dialog_SubAddCategory").dialog("close");
                        OnChange_GetSubCategory(trID);
                    }
                    else {
                        alert("Cannot add sub category");
                        $("#Dialog_SubAddCategory").dialog("close");
                    }
                }
                catch (e) { }

            },
            error: function () {
                alert("Adding sub category failed");
                $("#Dialog_SubAddCategory").dialog("close");
            }
        });
    }
}

function showMainCategory() {
    debugger;
    $("select#Select_Category").empty();
    $("table#CategoriesSelect tr:gt(" + 0 + ")").remove();
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "AdminHandler.asmx/ShowCategory_OnLoad",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                var catValue = null;
                if (result.Session == 0)// Session Expired
                {
                    window.location.href = "Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    m_arrCategory = result.Category;
                    $("select#Select_Category").append($('<option>', { value: "", text: "Select Category" }));
                    if (m_arrCategory.length > 0) {
                        for (i = 0; i < m_arrCategory.length; i++) {
                            catValue = m_arrCategory[i].nID;
                            var catName = m_arrCategory[i].sCategoryName;
                            $("select#Select_Category").append($('<option>', { value: catValue, text: catName }));
                        }
                    }
                }
                else {
                    $("select#Select_Category").append($('<option>', { value: "", text: "No category found" }));
                }
                if (catValue != null) {
                    Change_GetSubCategory();
                }
            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting categories");
        }
    });

}
var idVal = 1;
function OnChange_GetSubCategory(trID) {
    debugger;
    var rowIndex = $("table#CategoriesSelect tr#" + trID).index();
    $("table#CategoriesSelect tr:gt(" + rowIndex + ")").remove();
    if (trID == 0) {
        var sSelSubCategory = $("#Select_Category").val();
    }
    else {
        var sSelSubCategory = $("select#SelectFirstLevelCategories" + trID + " option:selected").val();
    }
    if (sSelSubCategory != "") {
        $.ajax({
            type: "POST",
            url: "AdminHandler.asmx/Change_GetSubCategory",
            data: '{"sSubCategory":"' + sSelSubCategory + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0)// Session Expired
                    {
                        window.location.href = "Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        m_arrSubCategory = result.SubCategory;
                        if (m_arrSubCategory.length > 0) {
                            var trRow = ' <tr id=' + idVal + '>';
                            trRow += '<td style="vertical-align: top;padding:10px">';
                            trRow += '<label class="custom-selectEvent">';
                            //trRow += '<select id="SelectFirstLevelCategories' + idVal + '" onchange="return OnChange_GetSubCategory(' + idVal + ');">';
                            trRow += '<select id="SelectFirstLevelCategories' + idVal + '" >';
                            trRow += '<option value="">Select Sub Category</option>';
                            for (i = 0; i < m_arrSubCategory.length; i++) {
                                var catValue = m_arrSubCategory[i].nID;
                                var catName = m_arrSubCategory[i].sSubCategoryName;
                                //$("select#Select_SubCategory").append($('<option>', { value: catValue, text: catName }));
                                trRow += '<option value="' + catValue + '">' + catName + '</option>';
                            }
                            trRow += '</select>';
                            trRow += '</label>';
                            trRow += '</td>';
                            trRow += '<td style="padding-left:0px">';
                            trRow += '<input type="button" id="btnAddCategory' + idVal + '" value="Add Category" style="float: left" onclick="return ShowAddSubCategoryDialog(' + trID + ');"/>';
                            trRow += '</td>';
                            if(trID!=1)
                            {                            
                            /* trRow += '<td style="padding-left:0px">';
                            trRow += '<input type="button" id="btnAddSubCategory' + idVal + '" value="Add Sub Category" style="float: left" onclick="return ShowAddSubCategoryDialog(' + idVal + ');"/>';
                            trRow += '</td>';*/
                            }
                            trRow += '<td style="padding-left:0px">';
                            trRow += '<input type="button" id="btnDelSubCategory' + idVal + '" value="Delete Sub-Category" style="float: left" onclick="return ShowDeleteCategoryDialog(' + idVal + ',' + trID + ');"/>';
                            trRow += '</td>';
                            trRow += '</tr>';
                            $("table#CategoriesSelect tbody").append(trRow);
                            idVal++;
                        }
                    }
                    else {
                        $("select#Select_SubCategory").append($('<option>', { value: "", text: "No category found" }));
                    }
                }
                catch (e) { }
            },
            error: function () {
                alert("Getting subCategory failed");
            }
        });
    }
    // alert($("#Select_Category option:selected").val());
}

function deleteCategory(CatToDelete, GetSubCat) {
    if (CatToDelete == 0) {
        var sParentCategory = $("select#Select_Category option:selected").val();
    }
    else {
        var sParentCategory = $("#SelectFirstLevelCategories" + CatToDelete + " option:selected").val();
    }
    if (sParentCategory == "") {
        alert("Please select a Category to Delete");
    }
    else {
        $.ajax({
            type: "POST",
            url: "AdminHandler.asmx/DeleteCategory",
            data: '{"sParentCategory":"' + sParentCategory + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0)// Session Expired
                    {
                        window.location.href = "Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        alert("Category deleted successfully");
                        $("#Dialog_DeleteCategory").dialog("close");
                        if (CatToDelete == 0) {
                            showMainCategory();
                        }
                        else {
                            OnChange_GetSubCategory(GetSubCat);
                        }
                    }
                    else {
                        alert("Cannot delete category");
                        $("#Dialog_DeleteCategory").dialog("close");
                    }
                }
                catch (e) { }
            },
            error: function () {

            }
        });
    }
}

function getProductDetailonChanegCategory(nCategoryID)
{
    $.ajax({
        type: "POST",
        url: "AdminHandler.asmx/getProductDetailonChanegCategory",
        data: "{'nCategoryID':'" + nCategoryID + "'}",
        contentType: "application/json; charset=UTF-8",
        datatype: "json",
        success: function (response) {

        },
        error: function () {

        }
    });
}