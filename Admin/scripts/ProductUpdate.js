﻿$(document).ready(function () {
    //Function to load all categories
    showAllCategories();

    $("#Dialog_AddProduct").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Product",
        modal: true,
        resizable: false,
        width: '700px',
        buttons: [
            {
                text: "Submit",
                click: function () {
                    addProduct();
                }
            }
        ]
    });
    
    $("#Dialog_UpdateCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Product",
        modal: true,
        resizable: false,
        width: '700px',
        buttons: [
            {
                text: "Submit",
                click: function () {
                    alert("Product Added sucessfully");
                    $(this).dialog("close");
                }
            }
        ]
    });

    $("#Dialog_GetProduct").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Product",
        modal: true,
        resizable: false,
        width: '700px',
        buttons: [
            {
                text: "Close",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

    $("#Dialog_SetProduct").dialog({
        autoOpen: false,
        draggable: true,
        title: "Add Product",
        modal: true,
        resizable: false,
        width: '700px',
        buttons: [
            {
                text: "Submit",
                click: function () {
                    setUpdateProduct(prID);
                }
            }
        ]
    });

    $("#Dialog_DeleteCategory").dialog({
        autoOpen: false,
        draggable: true,
        title: "Delete Product",
        modal: true,
        resizable: false,
        width: '700px',
        buttons: [
            {
                text: "Yes",
                click: function () {
                    DeleteProduct(ProductToDelete);
                }
            },
            {
                text: "NO",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

});

function ShowAddCategoryDialog() {
    debugger;
    $.ajax({
        type: "POST",
        url: "ImageHandler.asmx/GetRandomNo",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = response.d;
            $("#Hidden").val(result)
            //alert($("#Hidden").val())
            var nID = $("#productCategoryList option:selected").val();
            if (nID == "" || nID == undefined) {
                alert("Please select a category to add product");

                return;
                $("#Dialog_AddProduct").dialog("open");
                event.preventDefault();
            }
            else
            {
                $("#Dialog_AddProduct").dialog("open");
            }
            }

        });
   
    }
  

function editProductDialog() {
    $("#Dialog_UpdateCategory").dialog("open");
    event.preventDefault();
}
var ProductToDelete;
function ShowDeleteCategoryDialog(ProductId) { //ShowDeleteSubCategoryDialog
    ProductToDelete = ProductId;
    $("#Dialog_DeleteCategory").dialog("open");
    event.preventDefault();
}
function getProductDialog() {
 
    $("#Dialog_GetProduct").dialog("open");
    event.preventDefault();
}
var prID;
function setProductDialog(nID) {
    prID = nID;
    $("#Dialog_SetProduct").dialog("open");
    event.preventDefault();
}

function showAllCategories() {
    $.ajax({
        type: "POST",
        url: "ProductUpdateHandler.asmx/getsAllCategories",
        data: '{"ParentId":"' + 0 + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            successGetCategory(result, "");
        },
        error: function () {
            alert("Error");
        }

    });
}
function successGetCategory(result, ParentCat) {
    if (result.Category != undefined) {
        var s_Category = result.Category;
        if (s_Category.length > 0) {
            for (var i = 0; i < s_Category.length; i++) {
                var catValue = s_Category[i].nID;
                var catName = s_Category[i].sCategoryName;
                if (ParentCat != "") {
                    catName = ParentCat + ">" + catName;
                }
                $("select#productCategoryList").append($('<option>', { value: catValue, text: catName }));
                if ((s_Category[i].Category != undefined)) {
                    successGetCategory(s_Category[i]);
                }
                if ((s_Category[i].sSubcategory != undefined)) {
                    successGetCategory(s_Category[i], catName);
                }
            }
        }
    }
    else if (result.sSubcategory != undefined) {
        var s_Category = result.sSubcategory;
        if (s_Category.length > 0) {

            for (var i = 0; i < s_Category.length; i++) {
                // successGetCategory(s_Category[i]);
                var s_SubCategory = s_Category[i].Category;
                if (s_SubCategory.length > 0) {
                    for (var i = 0; i < s_SubCategory.length; i++) {
                        var SubcatValue = s_SubCategory[i].nID;
                        var SubcatName = s_SubCategory[i].sCategoryName;
                        if (ParentCat != "") {
                            SubcatName = ParentCat + ">" + SubcatName;
                        }
                        $("select#productCategoryList").append($('<option>', { value: SubcatValue, text: SubcatName }));
                        successGetCategory(s_SubCategory[i], SubcatName);
                    }
                }
            }
        }
    }
}

function addProduct() {
    debugger;
    var nID = $("#productCategoryList option:selected").val();
    if (nID == "" || nID == undefined) {
        alert("Please select a category to add product");
        $("#Dialog_AddProduct").dialog("close");
    }
    else {
        var productName = $("#text_ProductName").val();
        
        var productDescription = $("#Textarea_productDescription").val();
        
        //var adminComments = $("#text_adminComments").val();
        var sManufacturerPartNumber = $("#text_sManufacturerPartNumber").val();
        //var gin = $("#text_gin").val();
        //var additionalCharge = $("#text_additionalCharge").val();
        
        //var PhotoName = $("#photoName").val();        
        //var deliveryDates = $("#select_DeliveryDates option:selected").val();
        var fileUpload1 = $("#Image_Upload1").val();
        
        //var fileName = fileUpload.split(/~/);
        //alert(fileName[fileName.length-1])
        //var bPublish = $("#boolPublish").is(':checked')
        

        if (productName == "" || productName == undefined) {
            alert("Please add product name");
            $("#text_ProductName").focus();
            return false;
        }
       
        //else if (searchKeywords == "" || searchKeywords == undefined) {
        //    alert("Please add discount price");
        //    $("#Textarea_searchKeywords").focus();

        //}
        else if (sManufacturerPartNumber == "" || sManufacturerPartNumber == undefined) {
            alert("Please add product code");
            $("#text_sManufacturerPartNumber").focus();

        }
        //else if (additionalCharge == "" || additionalCharge == undefined) {
        //    alert("Please add additional charge");
        //    $("#text_additionalCharge").focus();

        //}
       
        else {
            var Data = {
                nID: nID,
                productName: productName,
                
                productDescription: productDescription,
                //adminComments: adminComments,
                sManufacturerPartNumber: sManufacturerPartNumber,
                //gin: gin,
                //width: width,
                //weight: weight,
                //length: length,
                //height: height,
                fileUpload: fileUpload1,
            }
            var Jason = JSON.stringify(Data);

            $.ajax({
                type: "POST",
                url: "ProductUpdateHandler.asmx/addProduct",
                data: Jason,
                //data: "{'nID':'" + nID + "','productName':'" + productName + "','productPrice':'" + productPrice + "','productDescription':'" + productDescription + "','searchKeywords':'" + searchKeywords + "','adminComments':'" + adminComments + "','sManufacturerPartNumber':'" + sManufacturerPartNumber + "','gin':'" + gin + "','weight':'" + weight + "','length':'" + length + "','width':'" + width + "','height':'" + height + "','fileUpload':'" + fileUpload + "','bPublish':'" + bPublish + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Default.aspx"; // Session end
                        return false;
                    }
                    if (result.retCode == 1) {
                        $("#Dialog_AddProduct").dialog("close");
                        alert("Product Add Successfully");
                        getProductSummary();
                    }
                },
                error: function () {

                }
            });
        }
    }
}

function getProductSummary()
{
    $("table#tblCategory tbody").empty();
    var nCatID = $("#productCategoryList option:selected").val();
    if (nCatID == "" || nCatID == undefined) {
        var tRow = '<tr>';
        tRow += '<td>';
        tRow += '<span>Select a category to see the products</span>';
        tRow += '</td></tr>'
        $("table#tblCategory tbody").append(tRow);
    }
    else {
        $.ajax({
            type: "POST",
            url: "ProductUpdateHandler.asmx/getProductSummary",
            data: '{"nCatID":"' + nCatID + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx"; // Session end
                    return false;
                }
                if (result.retCode == 1) {
                    var arr_CategorySummary = result.SategorySummary;
                    for (var i = 0; i < arr_CategorySummary.length; i++) {
                        var tRow = '<tr>';
                        tRow += '<td>';
                        var nID = arr_CategorySummary[i].nID;
                        var nCategoryID = arr_CategorySummary[i].nCategoryID;
                        var sCategoryName = arr_CategorySummary[i].sName;
                        tRow += "<tr>";
                        tRow += '<td style="width:300px"><span><b>' + arr_CategorySummary[i].sName + '</b></span></td>';
                        tRow += '<td style="width:200px"><span><b>' + arr_CategorySummary[i].sManufacturerNumber + '</b></span></td>';
                        tRow += '<td style="width:300px"><span><img  border="0" style="cursor:pointer" title="View Details" align="top" alt="" onclick="return ShowProduct(' + nID + ');">';
                        tRow += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/delete-btnNew.png" border="0" style="cursor:pointer" title="Delete Product" align="top" alt="" onclick="return ShowDeleteCategoryDialog(' + nID + ');">';
                        tRow += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  border="0" style="cursor:pointer" title="Add Product" align="top" alt="" onclick="return UpdateProduct(' + nID + ')"></span></td>';
                        tRow += "</tr>";
                        tRow += '<tr><td><br/></td></tr>';
                        $("table#tblCategory tbody").append(tRow);
                    }

                }
                else {
                    var tRow = '<tr>';
                    tRow += '<td>';
                    tRow += '<span>No Product to Display</span>';
                    tRow += '</td></tr>'
                    $("table#tblCategory tbody").append(tRow);
                }
            },
            error: function () {
                alert("Error");
            }

        });
    }
}

function DeleteProduct(nID) {
    $.ajax({
        type: "POST",
        url: "ProductUpdateHandler.asmx/DeleteProduct",
        data: '{"nID":"' + nID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx"; // Session end
                return false;
            }
            if (result.retCode == 1) {
                $("#Dialog_DeleteCategory").dialog("close");
                alert("Product deleted successfully");
                getProductSummary();
            }
        },
        error: function () {
            alert("Error in deleting ");
        }
    });
}

function ShowProduct(nID) {
    $.ajax({
        type: "POST",
        url: "ProductUpdateHandler.asmx/getUpdateProduct",
        data: '{"nID":"' + nID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx"; // Session end
                return false;
            }
            if (result.retCode == 1) {
                var arr_GetProductDetails = result.CategorySummary;
                for (var i = 0; i < 1; i++) {
                    $("#text_GetProductName").val(arr_GetProductDetails[i].sProductName);
                    $("#text_GetProductPrice").val(arr_GetProductDetails[i].sProductPrice);
                    $("#Textarea_GetproductDescription").val(arr_GetProductDetails[i].sProductDescription);
                    $("#Textarea_GetsearchKeywords").val(arr_GetProductDetails[i].sSearchKeywords);
                    //$("#text_GetadminComments").val(arr_GetProductDetails[i].sAdminKeywords);
                    $("#text_GetsManufacturerPartNumber").val(arr_GetProductDetails[i].sManufacturerNumber);
                    //$("#text_Getgin").val(arr_GetProductDetails[i].nGin);
                    //$("#text_GetadditionalCharge").val(arr_GetProductDetails[i].nAdditionalCharge);
                    //$("#text_Getweight").val(arr_GetProductDetails[i].nWeight);
                    //$("#text_Getlength").val(arr_GetProductDetails[i].nLength);
                    //$("#text_Getwidth").val(arr_GetProductDetails[i].sWidth);
                    //$("#text_Getheight").val(arr_GetProductDetails[i].nHeight);
                    //$("#text_GetDeliveryDates").val(arr_GetProductDetails[i].sDeliverydates);
                    if (arr_GetProductDetails[i].bMenLatest == 'True') {
                        $("#MenLatestD").attr("checked", true);
                    }
                    else
                    {
                        $("#MenLatestD").attr("checked", false);
                    }
                   
                    if (arr_GetProductDetails[i].bWomenLatest == 'True') {
                        $("#WomenLatestD").attr("checked", true);
                    }
                    else {
                        $("#WomenLatestD").attr("checked", false);
                    }
                    
                     if (arr_GetProductDetails[i].bKidsLatest == 'True') {
                        $("#KidsLatestD").attr("checked", true);
                     }
                     else {
                         $("#KidsLatestD").attr("checked", false);
                     }
                    getProductDialog(nID);
                }
            }
        },
        error: function () {
            alert("Error in deleting ");
        }
    });
}

function UpdateProduct(nID) {
    $.ajax({
        type: "POST",
        url: "ProductUpdateHandler.asmx/getUpdateProduct",
        data: '{"nID":"' + nID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx"; // Session end
                return false;
            }
            if (result.retCode == 1) {
                var arr_GetProductDetails = result.CategorySummary;
                for (var i = 0; i < 1; i++) {
                    $("#text_SetProductName").val(arr_GetProductDetails[i].sProductName);
                    $("#text_SetProductPrice").val(arr_GetProductDetails[i].sProductPrice);
                    $("#Textarea_SetproductDescription").val(arr_GetProductDetails[i].sProductDescription);
                    $("#Textarea_SetsearchKeywords").val(arr_GetProductDetails[i].sSearchKeywords);
                    //$("#text_SetadminComments").val(arr_GetProductDetails[i].sAdminKeywords);
                    $("#text_SetsManufacturerPartNumber").val(arr_GetProductDetails[i].sManufacturerNumber);
                    //$("#text_Setgin").val(arr_GetProductDetails[i].nGin);
                    //$("#text_SetadditionalCharge").val(arr_GetProductDetails[i].nAdditionalCharge);
                    //$("#text_Setweight").val(arr_GetProductDetails[i].nWeight);
                    //$("#text_Setlength").val(arr_GetProductDetails[i].nLength);
                    //$("#text_Setwidth").val(arr_GetProductDetails[i].sWidth);
                    //$("#text_Setheight").val(arr_GetProductDetails[i].nHeight);
                    if (arr_GetProductDetails[i].bMenLatest == 'True') {
                        $("#MenLatestU").attr("checked", true);
                    }
                    else {
                        $("#MenLatestU").attr("checked", false);
                    }

                    if (arr_GetProductDetails[i].bWomenLatest == 'True') {
                        $("#WomenLatesUt").attr("checked", true);
                    }
                    else {
                        $("#WomenLatesUt").attr("checked", false);
                    }

                    if (arr_GetProductDetails[i].bKidsLatest == 'True') {
                        $("#KidsLatestU").attr("checked", true);
                    }
                    else {
                        $("#KidsLatestU").attr("checked", false);
                    }
                    //var fileUpload5 = $("#Image_Upload1").val();
                    
                    //if (arr_GetProductDetails[i].sDeliverydates == '1-2 Days') {
                    //    $('#select_SetDeliveryDates option[value="1-2 Days"]').prop('selected', true);
                    //}
                    //else if (arr_GetProductDetails[i].sDeliverydates == '2-4 Days') {
                    //    $('#select_SetDeliveryDates option[value="2-4 Days"]').prop('selected', true);
                    //}
                    //else {
                    //    $('#select_SetDeliveryDates option[value="1 Week"]').prop('selected', true);
                    //}

                    //if (arr_GetProductDetails[i].bPublish == 'true' || arr_GetProductDetails[i].bPublish == 'True') {
                    //    $("#SetboolPublish").attr('checked', 'checked');
                    //}
                    //else {
                    //    $("#SetboolPublish").removeAttr('checked');
                    //} 
                   // $("#Image_SetUpload").val(arr_GetProductDetails[i].bPublish);
                    setProductDialog(nID);
                }
            }
        },
        error: function () {
            alert("Error in deleting ");
        }
    });
}

function setUpdateProduct(prID) {
    var nID = $("#productCategoryList option:selected").val();
    if (nID == "" || nID == undefined) {
        alert("Please select a category to add product");
        $("#Dialog_SetProduct").dialog("close");
    }
    else {
        var productName = $("#text_SetProductName").val();
        var productPrice = $("#text_SetProductPrice").val();
        var productDescription = $("#Textarea_SetproductDescription").val();
        var searchKeywords = $("#Textarea_SetsearchKeywords").val();
        //var adminComments = $("#text_SetadminComments").val();
        var sManufacturerPartNumber = $("#text_SetsManufacturerPartNumber").val();
        //var gin = $("#text_Setgin").val();
        //var additionalCharge = $("#text_SetadditionalCharge").val();
        //var weight = $("#text_Setweight").val();
        //var length = $("#text_Setlength").val();
        //var width = $("#text_Setwidth").val();
        //var height = $("#text_Setheight").val();
        //var deliveryDates = $("#select_SetDeliveryDates option:selected").val();
        //var fileUpload1 = $("#Image_UploadS1").val();
        //var fileUpload2 = $("#Image_UploadS2").val();
        //var fileUpload3 = $("#Image_UploadS3").val();
        var bMenLatest = $("#MenLatest").is(':checked')
        var bWomenLatest = $("#WomenLatest").is(':checked')
        var bKidsLatest = $("#KidsLatest").is(':checked')
        //var bPublish = $("#SetboolPublish").is(':checked')
        //alert(fileUpload1);
        if (productName == "" || productName == undefined) {
            alert("Please add product name");
            $("#text_SetProductName").focus();
        }
        else if (productPrice == "" || productPrice == undefined) {
            alert("Please add product price");
            $("#text_SetProductPrice").focus();
        }
        //else if (searchKeywords == "" || searchKeywords == undefined) {
        //    alert("Please add search keywords");
        //    $("#Textarea_SetsearchKeywords").focus();

        //}
        else if (sManufacturerPartNumber == "" || sManufacturerPartNumber == undefined) {
            alert("Please add pruduct code");
            $("#text_SetsManufacturerPartNumber").focus();

        }
        //else if (additionalCharge == "" || additionalCharge == undefined) {
        //    alert("Please add additional charge");
        //    $("#text_SetadditionalCharge").focus();

        //}
        //else if (weight == "" || weight == undefined) {
        //    alert("Please add wight of product");
        //    $("#text_Setweight").focus();

        //}
        //else if (length == "" || length == undefined) {
        //    alert("Please add length of product");
        //    $("#text_Setlength").focus();

        //}
        //else if (width == "" || width == undefined) {
        //    alert("Please add width of product");
        //    $("#text_Setwidth").focus();

        //}
        //else if (height == "" || height == undefined) {
        //    alert("Please add height of product");
        //    $("#text_Setheight").focus();
        //}
        else {
            var Data = {
                prID:prID,
                nID: nID,
                productName: productName,
                productPrice: productPrice,
                productDescription: productDescription,
                searchKeywords: searchKeywords,
                //adminComments: adminComments,
                sManufacturerPartNumber: sManufacturerPartNumber,
                //gin: gin,
                //width: width,
                //weight: weight,
                //length: length,
                //height: height,
                //fileUpload: fileUpload1,
                //image1: fileUpload2,
                //image2: fileUpload3,
                //bPublish: bPublish,
                bMenLatest: bMenLatest,
                bWomenLatest: bWomenLatest,
                bKidsLatest: bKidsLatest,
            }
            var Jason = JSON.stringify(Data);
            $.ajax({
                type: "POST",
                url: "ProductUpdateHandler.asmx/SetUpdateProduct",
                data: Jason,
                //data: "{'prID':'" + prID + "','nID':'" + nID + "','productName':'" + productName + "','productPrice':'" + productPrice + "','productDescription':'" + productDescription + "','searchKeywords':'" + searchKeywords + "','adminComments':'" + adminComments + "','sManufacturerPartNumber':'" + sManufacturerPartNumber + "','gin':'" + gin + "','additionalCharge':'" + additionalCharge + "','weight':'" + weight + "','length':'" + length + "','width':'" + width + "','height':'" + height + "','fileUpload':'" + fileUpload + "','bMenLatest':'" + bMenLatest + "','bWomenLatest':'" + bWomenLatest + "','bKidsLatest':'" + bKidsLatest + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "Default.aspx"; // Session end
                        return false;
                    }
                    if (result.retCode == 1) {
                        $("#Dialog_SetProduct").dialog("close");
                        getProductSummary();
                    }
                },
                error: function () {

                }
            });
        }
    }
}