﻿$(document).ready(function () {
    getProduct();
   
});

function getProduct() {
    debugger;
    $("select#Select_Product").empty();
    //$("table#CategoriesSelect tr:gt(" + 0 + ")").remove();
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "AdminHandler.asmx/ShowProduct_OnLoad",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                var catValue = null;
                if (result.Session == 0)// Session Expired
                {
                    window.location.href = "Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    //m_arrCategory = result.Category;
                    var product = result.tbl_Product;
                    $("select#Select_Product").append($('<option>', { value: "", text: "Select Product" }));
                    if (product.length > 0) {
                        for (i = 0; i < product.length; i++) {
                            debugger;
                            catValue = product[i].nID;
                            var catName = product[i].sProductName;
                            $("select#Select_Product").append($('<option>', { value: catValue, text: catName }));
                        }
                    }
                }
                else {
                    $("select#Select_Product").append($('<option>', { value: "", text: "No Product found" }));
                }
                //if (catValue != null) {
                //    Change_GetSubCategory();
                //}
            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting Products");
        }
    });

}

function getProductSummary() {
    debugger;
   
    var nID = $("#Select_Product").val();
    if (nID == "" || nID == undefined) {
        var tRow = '<tr>';
        tRow += '<td>';
        tRow += '<span>Select a Product to see the comments</span>';
        tRow += '</td></tr>'
        $("#tblProduct tbody").append(tRow);
    }
    else {
        $.ajax({
            type: "POST",
            url: "AdminHandler.asmx/getProductSummary",
            data: '{"nID":"' + nID + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx"; // Session end
                    return false;
                }
                if (result.retCode == 1) {
                    $("#tblProduct tbody").empty();
                    $("#tblComment tbody").empty();
                    var arr_productSummary = result.tbl_Product;
                    var tRow;
                    var seprater;
                    var AllComm;
                    for (var i = 0; i < arr_productSummary.length; i++) {
                        tRow += '<tr>';
                        tRow += '<td>';
                        var nID = arr_productSummary[i].Product_id;
                        //var nCategoryID = arr_productSummary[i].nID;
                        var sCategoryName = arr_productSummary[i].Comment;
                       
                         AllComm += arr_productSummary[i].Comment+", ";
                        tRow += "<tr>";
                        tRow += '<td style="width:98%"><span><b>' + arr_productSummary[i].Comment + '</b></span></td>';
                        //tRow += '<td style="width:200px"><span><b>' + arr_CategorySummary[i].nID + '</b></span></td>';
                        tRow += '<td style="width:2%"><span><img src="images/analysis.png" border="0" style="cursor:pointer" title="Analysis" align="top" alt="" onclick="AnalysisComment(\'' + arr_productSummary[i].Comment + '\')">';
                        //tRow += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/delete-btnNew.png" border="0" style="cursor:pointer" title="Delete Product" align="top" alt="" onclick="return ShowDeleteCategoryDialog(' + nID + ');">';
                        //tRow += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/edit-btnNew.png" border="0" style="cursor:pointer" title="Add Product" align="top" alt="" onclick="return UpdateProduct(' + nID + ')"></span></td>';
                        tRow += "</tr>";
                        tRow += '<tr><td><br/></td></tr>';
                       
                    }
                    var AllComments = AllComm.split('undefined');
                    //alert(s[1]);

                    tRow += '<div class="tblSeparator"></div><br/>';
                    seprater += '<tr id="clear">';
                    seprater += '<td style="width:90%"><span><b> Confidence: </b></span><span id="Confi"><b></b></span><br/><span><b> Sentiment: </b></span><span id="Senti"><b></b></span></td>';
                    //seprater += '<td style="width:90%"></td>';
                    seprater += '<td style="width:10%"><span><img src="images/analysisAll.png" border="0" style="margin-left: 100%;cursor:pointer" title="Analysis All" align="top" alt="" onclick="AllComment(\'' + AllComments + '\')")">';
                   
                    seprater += "</tr>";
                    $("#tblComment tbody").append(seprater);
                    $("#tblProduct tbody").append(tRow);
                    
                }
                else {
                    //alert("No Comments");
                    $("#tblProduct tbody").empty();
                    $("#tblComment tbody").empty();
                    var tRow = '<tr>';
                    tRow += '<td>';
                    tRow += '<span>No Product Comment to Display</span>';
                    tRow += '</td></tr>'
                    $("#tbl_Product tbody").append(tRow);
                }
            },
            error: function () {
                alert("Error");
            }

        });
    }
}


function AnalysisComment(comment)
{   
    $.ajax({
        type: 'POST',
        url: 'AdminHandler.asmx/Analysis',
        data: '{"Comment":"' + comment + '"}',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx"; // Session end
                return false;
            }
            $("#Confi").empty();
            $("#Senti").empty();
           
            if (result.retCode == 1) {
                var confidence = result.ret_Result;
                var Result = confidence[0].result;
                var Con = Result["confidence"];
                var Sen = Result["sentiment"];

                $("#Confi").append(Con);
                $("#Senti").append(Sen);
                //alert(Result["confidence"]);

            }
           
        },
        error: function () {
            alert("Error");
        }
    });
}

function AllComment(comment) {
    debugger;
    alert(comment);
    $.ajax({
        type: 'POST',
        url: 'AdminHandler.asmx/Analysis',
        data: '{"Comment":"' + comment + '"}',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx"; // Session end
                return false;
            }
            $("#Confi").empty();
            $("#Senti").empty();

            if (result.retCode == 1) {
                var confidence = result.ret_Result;
                var Result = confidence[0].result;
                var Con = Result["confidence"];
                var Sen = Result["sentiment"];

                $("#Confi").append(Con);
                $("#Senti").append(Sen);
                //alert(Result["confidence"]);

            }

        },
        error: function () {
            alert("Error");
        }
    });
}