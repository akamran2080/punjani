﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductUpdate.aspx.cs" Inherits="Admin_ProductUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="scripts/ProductUpdate.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type=checkbox]').click(function () {
                var groupName = $(this).attr('groupname');

                if (!groupName)
                    return;

                var checked = $(this).is(':checked');

                $("input[groupname='" + groupName + "']:checked").each(function () {
                    $(this).prop('checked', '');
                });

                if (checked)
                    $(this).prop('checked', 'checked');
            });
        });
</script>

    <script type="text/javascript">

        function Uploading1() {
            debugger;
            var fileUpload = $("#Image_Upload1").get(0);
            var files = fileUpload.files;
            var test = new FormData();
            for (var i = 0; i < files.length; i++) {
                test.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "UploadHandler.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: test,
                // dataType: "json",
                success: function (result) {
                    alert(result);
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }
        $(function () {
            $('#btnUpload').click(function () {

            });
        })
    </script>
   
    <table cellpadding="0" cellspacing="0" border="0" width="982px">
        <tr width="982px">
            <td colspan="2"></td>
        </tr>
        <tr style="height: 1px">
            <td colspan="2"></td>
        </tr>
        <tr>
            <td class="admin-content">
                <div class="row1">
                    <div>
                        <div class="searchCountry">
                            <label class="custom-selectTopic">
                                <select id="productCategoryList" name="productCategoryList" onchange="getProductSummary();">
                                    <option value="">Select Category</option>
                                </select>
                            </label>
                        </div>
                        <div class="addnewCat">
                            <img src="images/max_icon.png" alt="" title="Add Product" style="cursor: pointer; margin-top: 5px"
                                onclick="return ShowAddCategoryDialog();" />
                        </div>
                    </div>
                </div>
                <br />
            </td>
            <tr>
                <td class="admin-content">
                    <div class="admin-tblHeader">
                        <table class="DisplayTableDashboard" style="width:500px">
                            <tr>
                                <td style="width:300px"><span class="admin-tblHeaderName">Product Name</span></td>
                                <td style="width:200px"><span class="admin-tblHeaderName">Product Code</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tblList">
                        <table id="tblCategory" class="DisplayTableDashboard" border="0" style="width:800px">
                            <tr>
                                <td><span>Select a category to see the products.</span></td>
                            </tr>
                        </table>
                        <div class="tblSeparator"></div>
                    </div>
                    

                </td>
                <td>
                    <img src="images/spacer.gif" width="1" height="388" alt="" />
                </td>
            </tr>
    </table>

    <div id="Add_Category" style="display: none">
        <div class="row1subscriptionpopup">

            <div class="popupdiv">
                <table width="90%" cellpadding="0" cellspacing="0" class="MainTable">
                    <tr>
                        <td width="200">Category Name
                                    <asp:HiddenField ID="HiddenField_Value" runat="server" Value="0"></asp:HiddenField>
                            <asp:HiddenField ID="HiddenField_ValueChild" runat="server" Value="0"></asp:HiddenField>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox_Category" runat="server" CssClass="TextBox_CategoryName"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <td width="200">Allow Subscription
                        </td>
                        <td>
                            <input type="checkbox" id="chkAllowSubcription" name="chkAllowSubcription" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            Subcription Packagese<br />
                            <div class="admin-popuptable">
                                <div style="float: left;"><span class="admin-tblHeaderTest">No. of Test</span></div>
                                <div style="float: left;"><span class="admin-tblHeaderPrice">Price (USD)</span></div>
                                <div style="float: right; padding-right: 15px;">
                                    <span class="admin-tblHeaderDelete">
                                        <img src="images/add-btnNew.png" onclick="return  Table_AddBanner_addRow();" /></span>
                                </div>
                                <%--<span class="admin-tblHeaderTest">No. of Test</span> <span class="admin-tblHeaderPrice">
                                            Price (USD) </span><span class="admin-tblHeaderDelete">
                                                <img src="images/upload_question.png" onclick="return Table_AddBanner_addRow();" /></span>--%>
                            </div>
                            <table id="Table_SubcriptionPrice" cellpadding="0" cellspacing="0" border="0" class="DisplayTable"
                                style="width: 430px">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" name="text_NOT" id="text_NOT" class="TextBox_CategoryNOT" onkeypress="return onlyNumbers(this,event);" />
                                        </td>
                                        <td style="padding-left: 55px">
                                            <input type="text" name="text_price" id="text_price" class="TextBox_CategoryNOT" onkeypress="return onlyNumbers(this,event);" />
                                        </td>
                                        <td style="padding-left: 10px">
                                            <img src="images/delete-btnNew.png" onclick="return Table_removeRow($(this))" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%--<asp:Button ID="submit" Text="Add" runat="server" OnClientClick="return AddCategory();" />--%>
                            <input type="button" title="Add" value="Add" id="buttonsubmit" onclick="return AddCategory();" />
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>

    

    <div id="Dialog_AddProduct" >
        <div class="row1subscriptionpopup222">
            <div class="popupdiv">
                
                <table id="Table1" class="MainTable" style="margin-right: 20px">
                    <tr>
                        <td colspan="2" style="padding-left: 40px;">Enter the product details and press the submit button
                        </td>
                    </tr>

                    <tr>
                        <td width="150" style="padding-left: 40px">Product Code
                        </td>
                        <td width="200">
                            <input id="text_sManufacturerPartNumber" type="text" name="text_sManufacturerPartNumber" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Name
                        </td>
                        <td width="200">
                            <input id="text_ProductName" type="text" name="text_ProductName" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                   
                   
                    
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Description
                        </td>
                        <td>
                            <textarea style="height: 100px" rows="6" cols="50" id="Textarea_productDescription" name="Textarea_productDescription" class="TextBox_CategoryTestName"></textarea>
                        </td>
                    </tr>
                   
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Admin Comments
                        </td>
                        <td>
                            <textarea style="height: 80px" rows="4" cols="50" id="text_adminComments" name="text_adminComments" class="TextBox_CategoryTestName"></textarea>
                        </td>
                    </tr>--%>
                    
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">GTIN (global trade item number)
                        </td>
                        <td width="200">
                            <input id="text_gin" type="text" name="text_gin" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Additional shipping charge
                        </td>
                        <td width="200">
                            <input id="text_additionalCharge" type="text" name="text_additionalCharge" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>--%>
                    
                    
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Delivery Days
                        </td>
                        <td width="200">
                            <select id="select_DeliveryDates">
                                <option>1-2 Days</option>
                                <option>2-4 Days</option>
                                <option>1 Week</option>


                            </select>
                        </td>
                    </tr>--%>
                     
                    <tr>
                        <td width="150" style="padding-left: 40px">Upload Picture1
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <input type="file" onchange="Uploading1()" id="Image_Upload1" />
                        </td>
                        
                        <td>

                           <%-- <asp:HiddenField ID="HiddenField_CatLev_ID" runat="server" Value="0"></asp:HiddenField>--%>
                       <%-- <asp:Label runat="server"  ClientIDMode="Static" Value=""></asp:Label>
                            <asp:FileUpload runat="server" ID="UploadControl1" ClientIDMode="Static" onchange="this.form.submit" EnableTheming="false" OnUnload="Upload" ></asp:FileUpload>
                            <asp:Button ID="Button4" Text="Upload" runat="server" Onclick="Upload" Style="display: none" />
                            <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Upload" Style="display: none"  />
                                                           <script type="text/javascript">
                                                               function UploadFile(fileUpload) {
                                                                   debugger;
                                                                   if (fileUpload.value != '') {
                                                                       document.getElementById("<%=Button2.ClientID %>").click();
                                                                   }
                                                               }
                                                           </script>--%>
                       <%-- <input type="file" id="UploadControl1"  width="200" style="padding-left: 25px;" name="Upload" onchange="Uploading()"  aria-invalid="false"></span>--%>
                            </td>
                    </tr>
                    
                   

                   
                    
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Publish
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                                <input type="checkbox" id="boolPublish" name="boolPublish" />
                                <label for="boolPublish"></label>
                            </div>
                        </td>
                    </tr>--%>
                   
                </table>
                      
                    
            </div>
        </div>
    </div>

         
    <div id="Dialog_GetProduct" style="display: none">
        <div class="row1subscriptionpopup222">
            <div class="popupdiv">
                <table id="Table1" class="MainTable" style="margin-right: 20px">
                    <tr>
                        <td colspan="2" style="padding-left: 40px">Product Details
                        </td>
                    </tr>
                     <tr>
                        <td width="150" style="padding-left: 40px">Product Code
                        </td>
                        <td width="200">
                            <input id="text_GetsManufacturerPartNumber" type="text" name="text_sManufacturerPartNumber" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Name
                        </td>
                        <td width="200">
                            <input id="text_GetProductName" type="text" name="text_GetProductName" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Price
                        </td>
                        <td width="200">
                            <input id="text_GetProductPrice" type="text" name="text_ProductPrice" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <%-- Product Search Keywords--%>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Discount Price
                        </td>
                        <td>
                            <input id="Textarea_GetsearchKeywords" name="Textarea_searchKeywords" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Description
                        </td>
                        <td>
                            <textarea style="height: 100px" rows="6" cols="50" id="Textarea_GetproductDescription" name="Textarea_productDescription" class="TextBox_CategoryTestName" readonly></textarea>
                        </td>
                    </tr>
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Admin Comments
                        </td>
                        <td>
                            <textarea style="height: 80px" rows="4" cols="50" id="text_GetadminComments" name="text_adminComments" class="TextBox_CategoryTestName" readonly></textarea>
                        </td>
                    </tr>                   
                    <tr>
                        <td width="150" style="padding-left: 40px">GTIN (global trade item number)
                        </td>
                        <td width="200">
                            <input id="text_Getgin" type="text" name="text_gin" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Additional shipping charge
                        </td>
                        <td width="200">
                            <input id="text_GetadditionalCharge" type="text" name="text_additionalCharge" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Weight
                        </td>
                        <td width="200">
                            <input id="text_Getweight" type="text" name="text_weight" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Length
                        </td>
                        <td width="200">
                            <input id="text_Getlength" type="text" name="text_length" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Width
                        </td>
                        <td width="200">
                            <input id="text_Getwidth" type="text" name="text_width" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Height
                        </td>
                        <td width="200">
                            <input id="text_Getheight" type="text" name="text_height" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>--%>
                     <tr>
                        <td width="150" style="padding-left: 40px">Eye Wears   
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="MenLatestD" groupname="groupD" value="1" name="Latest" />
                                <label for="MenLatest"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Contact Lenses
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="WomenLatestD" groupname="groupD" value="2" name="Latest" />
                                <label for="WomenLatest"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Eye Testing Machines
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="KidsLatestD" groupname="groupD" value="3" name="Latest" />
                                <label for="KidsLatest"></label>
                            </div>
                        </td>
                    </tr>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Delivery Days
                        </td>
                        <td width="200">
                            <input id="text_GetDeliveryDates" type="text" name="text_height" class="TextBox_CategoryTestName" readonly />
                        </td>
                    </tr>--%>

                   <%-- <tr>
                        <td width="150" style="padding-left: 40px">Publish
                        </td>
                        <td width="200"  style="padding-left: 25px">
                            <div class="squaredTwo" style="float: left">
                                <input type="checkbox" id="GetboolPublish" name="boolPublish" />
                                <label for="boolPublish"></label>
                            </div>
                        </td>
                    </tr>--%>
                   
                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                </table>
            </div>
        </div>
    </div>

    <div id="Dialog_SetProduct" style="display: none">
        <div class="row1subscriptionpopup222">
            <div class="popupdiv">
                <table id="Table_SetCategory" class="MainTable" style="margin-right: 20px">
                    <tr>
                        <td colspan="2">Enter the product details and press the submit button
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Code
                        </td>
                        <td width="200">
                            <input id="text_SetsManufacturerPartNumber" type="text" name="text_sManufacturerPartNumber" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Name
                        </td>
                        <td width="200">
                            <input id="text_SetProductName" type="text" name="text_ProductName" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Price
                        </td>
                        <td width="200">
                            <input id="text_SetProductPrice" type="text" name="text_ProductPrice" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Discount Price
                        </td>
                        <td>
                            <input id="Textarea_SetsearchKeywords" name="Textarea_searchKeywords" class="TextBox_CategoryTestName"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Product Description
                        </td>
                        <td>
                            <textarea style="height: 100px" rows="6" cols="50" id="Textarea_SetproductDescription" name="Textarea_productDescription" class="TextBox_CategoryTestName"></textarea>
                        </td>
                    </tr>
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Admin Comments
                        </td>
                        <td>
                            <textarea style="height: 80px" rows="4" cols="50" id="text_SetadminComments" name="text_adminComments" class="TextBox_CategoryTestName"></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="150" style="padding-left: 40px">GTIN (global trade item number)
                        </td>
                        <td width="200">
                            <input id="text_Setgin" type="text" name="text_gin" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Additional shipping charge
                        </td>
                        <td width="200">
                            <input id="text_SetadditionalCharge" type="text" name="text_additionalCharge" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 40px">Weight
                        </td>
                        <td width="200">
                            <input id="text_Setweight" type="text" name="text_weight" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Length
                        </td>
                        <td width="200">
                            <input id="text_Setlength" type="text" name="text_length" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Width
                        </td>
                        <td width="200">
                            <input id="text_Setwidth" type="text" name="text_width" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Height
                        </td>
                        <td width="200">
                            <input id="text_Setheight" type="text" name="text_height" class="TextBox_CategoryTestName" />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Delivery Days
                        </td>
                        <td width="200">
                            <select id="select_SetDeliveryDates">
                                <option value="1-2 Days">1-2 Days</option>
                                <option value="2-4 Days">2-4 Days</option>
                                <option value="1 Week">1 Week</option>
                            </select>
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="150" style="padding-left: 10px">Upload Picture
                        </td>
                        <td width="200">
                            <input type="file" id="Image_SetUpload" />
                        </td>
                    </tr>--%>

                     <tr>
                        <td width="150" style="padding-left: 40px">Upload Picture1
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <input type="file" onchange="Uploading1()" id="Image_UploadS1" />
                        </td>
                        
                        <td>

                           <%-- <asp:HiddenField ID="HiddenField_CatLev_ID" runat="server" Value="0"></asp:HiddenField>--%>
                       <%-- <asp:Label runat="server"  ClientIDMode="Static" Value=""></asp:Label>
                            <asp:FileUpload runat="server" ID="UploadControl1" ClientIDMode="Static" onchange="this.form.submit" EnableTheming="false" OnUnload="Upload" ></asp:FileUpload>
                            <asp:Button ID="Button4" Text="Upload" runat="server" Onclick="Upload" Style="display: none" />
                            <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Upload" Style="display: none"  />
                                                           <script type="text/javascript">
                                                               function UploadFile(fileUpload) {
                                                                   debugger;
                                                                   if (fileUpload.value != '') {
                                                                       document.getElementById("<%=Button2.ClientID %>").click();
                                                                   }
                                                               }
                                                           </script>--%>
                       <%-- <input type="file" id="UploadControl1"  width="200" style="padding-left: 25px;" name="Upload" onchange="Uploading()"  aria-invalid="false"></span>--%>
                            </td>
                    </tr>
                    
                    <tr>
                        <td width="150" style="padding-left: 40px">Upload Picture2
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <input type="file" onchange="Uploading2()" id="Image_UploadS2" />
                        </td>
                        
                      
                       <%-- <td>
                        <asp:Label runat="server"  ClientIDMode="Static" Value=""></asp:Label>
                        <asp:FileUpload runat="server" onchange="Uploading2()" ID="UploadControl2"></asp:FileUpload>
                            </td>--%>
                    </tr>

                    <tr>
                         
                        <td width="150" style="padding-left: 40px">Upload Picture3
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <input type="file" onchange="Uploading3()" id="Image_UploadS3" />
                        </td>
                        <%--<td>
                        <asp:Label runat="server" ID="Hidden"  ClientIDMode="Static" Value=""></asp:Label>
                       <asp:FileUpload runat="server" onchange="Uploading3()" ID="UploadControl3"></asp:FileUpload>
                            </td>--%>
                    </tr>
                     <tr>
                        <td width="150" style="padding-left: 40px">Eye Wears  
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="MenLatestU" groupname="groupU" value="1" name="Latest" />
                                <label for="MenLatest"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Contact Lenses
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="WomenLatestU" groupname="groupU" value="2" name="Latest" />
                                <label for="WomenLatest"></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" style="padding-left: 40px">Eye Testing Machines
                        </td>
                        <td width="200" style="padding-left: 25px;">
                            <div class="squaredTwo" style="float: left">
                            <input type="checkbox" id="KidsLatestU" groupname="groupU" value="3" name="Latest" />
                                <label for="KidsLatest"></label>
                            </div>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td width="150" style="padding-left: 40px">Publish
                        </td>
                        <td width="200" style="padding-left: 25px">
                            <div class="squaredTwo" style="float: left" >
                                <input type="checkbox" id="SetboolPublish" name="SetboolPublish" />
                                <label for="SetboolPublish"></label>
                            </div>
                        </td>
                    </tr>--%>
                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                </table>
            </div>
        </div>
    </div>

    <div id="Dialog_DeleteCategory" style="display: none">
        <div class="row1subscriptionpopup222">
            <div class="popupdiv">
                <table id="Table_DeleteCategory" class="MainTable" style="margin-right: 20px">
                    <tr>
                        <td colspan="2">Are you sure want to delete the product?? 
                        </td>
                    </tr>

                    <%--<tr>
                                        <td colspan="2">
                                            <input type="button" title="Add" value="Add" id="btnSubmit" onclick="return AddSubject();" />
                                        </td>
                                    </tr>--%>
                </table>
            </div>
        </div>
    </div>

    <div id="Dialog_UpdateCategory" style="display: none">
        <div class="row1subscriptionpopup">

            <div class="popupdiv">
                <table width="90%" cellpadding="0" cellspacing="0" class="MainTable">
                    <tr>
                        <td style="width: 40%">Category Name
                                  
                        </td>
                        <td style="width: 60%">
                            <asp:TextBox ID="TextBox_UpdateCategory" runat="server" CssClass="TextBox_CategoryNameUpdate"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Allow Subscription
                        </td>
                        <td>
                            <input type="checkbox" id="chkAllowSubcriptionUpdate" name="chkAllowSubcriptionUpdate" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            Subcription Packagese<br />
                            <div class="admin-popuptable">
                                <div style="float: left;"><span class="admin-tblHeaderTest">No. of Test</span></div>
                                <div style="float: left;"><span class="admin-tblHeaderPrice">Price (USD)</span></div>
                                <div style="float: right; padding-right: 15px;">
                                    <span class="admin-tblHeaderDelete">
                                        <img src="images/add-btnNew.png" onclick="return Table_AddCatSbscription_addRow();" /></span>
                                </div>
                                <%--<span class="admin-tblHeaderTest" style="padding-bottom:-7px;">No. of Test</span> <span class="admin-tblHeaderPrice">
                                            Price (USD) </span><span class="admin-tblHeaderDelete">
                                                <img src="images/upload_question.png" onclick="return Table_AddCatSbscription_addRow();" /></span>--%>
                            </div>
                            <table id="tbl_UpdateCategorySubcription" class="DisplayTable" style="width: 430px">
                                <tbody>
                                    <%-- <tr>
                                            <td>
                                                <input type="text" name="text_NOT" id="text1" />
                                            </td>
                                            <td>
                                                <input type="text" name="text_price" id="text2" />
                                            </td>
                                            <td>
                                                <img src="images/cross.png" onclick="return Table_removeRow($(this))" />
                                            </td>
                                        </tr>--%>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="button" id="Button1" value="Update" onclick="return UpdateCategorySubscriptionMain();" />
                            <%--<asp:Button ID="Button1" Text="Update" runat="server" OnClientClick="return UpdateCategorySubscriptionMain();" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div id="Add_Test" style="display: none">
        <div class="row1subscriptionpopup">

            <table cellpadding="0" cellspacing="0" width="90%" class="MainTable">
                <tr>
                    <td colspan="2" align="left" style="width: 30%">Country
                    </td>
                    <td style="width: 70%">
                        <asp:Label ID="Label_Country" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">Category
                    </td>
                    <td>
                        <asp:Label ID="Label_Category" runat="server"></asp:Label>
                        <asp:HiddenField ID="HiddenFieldCategory" runat="server" Value="0"></asp:HiddenField>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Test Name <span class="mandatory">*</span>
                    </td>
                    <td>
                        <input type="text" id="TestName" class="TextBox_CategoryTestName" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Time Duration<span class="mandatory">*</span>
                    </td>
                    <td>
                        <label class="custom-selecturation">
                            <select id="Select_Duration">
                                <option value="0">Select</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="25">25</option>
                                <option value="30">30</option>
                                <option value="35">35</option>
                                <option value="40">40</option>
                                <option value="45">45</option>
                                <option value="50">50</option>
                                <option value="55">55</option>
                                <option value="60">60</option>
                            </select>
                        </label>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <div class="admin-popuptableTopic">
                            <span class="admin-tblHeaderTest">Topic Name</span> <span class="category-Question">No of Questions </span>
                        </div>
                        <table id="TopicQuestion" cellpadding="0" cellspacing="0" class="DisplayTableTooic"
                            style="display: none">
                            <tbody>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="Select Topic" onclick="return ShowTopicList();" />
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div id="DialogBox_TopicList" style="display: none">
        <div class="row1subscriptionpopup">

            <table cellpadding="0" cellspacing="0" width="90%" class="MainTable">
                <tr>
                    <td>Enter Search Topic
                    </td>
                    <td>
                        <input type="text" name="textTopicList" id="textTopicList" class="TextBox_CategorysearchTopic" onkeypress="return GetTopicListKeyPress(event.keyCode);" />
                    </td>
                    <td>
                        <input type="button" value="Search" onclick="return GetTopicList();" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="admin-popuptableTopicName">
                            <span class="admin-tblHeaderTest"></span><span class="category-TopicName">Topic Name</span>
                        </div>
                        <table id="Table_TopicList" cellpadding="0" cellspacing="0" class="DisplayTableTooic"
                            style="display: none">
                            <tbody>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="Add To Topic" onclick="return addToTopicList();" />
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div id="AddLogo" style="display: none">
        <div class="row1subscriptionpopup">

            <div class="popupdiv">
                <table id="tblCategoryImage" class="MainTable" style="margin-right: 20px">

                    <tr>
                        <td width="150">Add Icon
                                     <asp:HiddenField ID="HiddenFieldCatID_Value" runat="server" />
                            <asp:HiddenField ID="HiddenField_CountryId" runat="server" />
                        </td>
                        <td width="200">
                            <input id="FileUpload" type="file" name="FileUpload" />

                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">Discription<br />
                            <input id="Htmleditor" runat="server" name="Htmleditor" type="text" class="jqte-test" style="margin-left: 20px" />
                            <%-- <input type="text" name="discription" />--%>
                        </td>

                    </tr>
                </table>
                <table class="MainTable">
                    <tr>
                        <td colspan="2">
                            <input type="submit" title="Add" value="Add" id="btnSubmit" onclick="return addProduct();" />
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</asp:Content>

