﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/scripts.js"></script>
    <script src="assets/js/slider_revolution.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="wrapper">

        <!-- REVOLUTION SLIDER -->
        <!--<div class="fullwidthbanner-container roundedcorners">
            <div class="fullwidthbanner">
                <ul>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                        <img src="assets/images/dummy.png" alt="church3" data-lazyload="assets/images/demo/home/medical_1.jpg" data-fullwidthcentering="on">
                        <div class="tp-caption medium_text lft"
                        data-x="90"
                        data-y="180"
                        data-speed="300"
                        data-start="500"
                        data-easing="easeOutExpo">Pediatric Care
                        </div>

                        <div class="tp-caption large_text lfb"
                        data-x="90"
                        data-y="222"
                        data-speed="300"
                        data-start="800"
                        data-easing="easeOutExpo">A wide spectre of quality<br /> medical services
                        </div>

                        <div class="tp-caption lfb"
                        data-x="90"
                        data-y="330"
                        data-speed="300"
                        data-start="1100"
                        data-easing="easeOutExpo">
                        <a href="https://wrapbootstrap.com/theme/atropos-responsive-website-template-WB05SR527" target="_blank" class="btn btn-primary btn-lg">Buy This Theme</a>
                        </div>
                    </li>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                        <img src="assets/images/dummy.png"  alt="church1" data-lazyload="assets/images/demo/home/medical_2.jpg" data-fullwidthcentering="on">
                        <div class="tp-caption medium_text lft"
                        data-x="90"
                        data-y="180"
                        data-speed="300"
                        data-start="500"
                        data-easing="easeOutExpo">Full Treatment
                        </div>

                        <div class="tp-caption large_text lfb"
                        data-x="90"
                        data-y="222"
                        data-speed="300"
                        data-start="800"
                        data-easing="easeOutExpo">A wide spectre of quality<br /> medical services
                        </div>

                        <div class="tp-caption lfb"
                        data-x="90"
                        data-y="330"
                        data-speed="300"
                        data-start="1100"
                        data-easing="easeOutExpo">
                        <a href="https://wrapbootstrap.com/theme/atropos-responsive-website-template-WB05SR527" target="_blank" class="btn btn-primary btn-lg">Buy This Theme</a>
                        </div>
                    </li>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                        <img src="assets/images/dummy.png"  alt="church2" data-lazyload="assets/images/demo/home/medical_3.jpg" data-fullwidthcentering="on">
                        <div class="tp-caption medium_text lft"
                        data-x="90"
                        data-y="180"
                        data-speed="300"
                        data-start="500"
                        data-easing="easeOutExpo">Pediatric Care
                        </div>

                        <div class="tp-caption large_text lfb"
                        data-x="90"
                        data-y="222"
                        data-speed="300"
                        data-start="800"
                        data-easing="easeOutExpo">A wide spectre of quality<br /> medical services
                        </div>

                        <div class="tp-caption lfb"
                        data-x="90"
                        data-y="330"
                        data-speed="300"
                        data-start="1100"
                        data-easing="easeOutExpo">
                        <a href="https://wrapbootstrap.com/theme/atropos-responsive-website-template-WB05SR527" target="_blank" class="btn btn-primary btn-lg">Buy This Theme</a>
                        </div>
                    </li>

                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>-->


        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner ken-burns">
                <ul>

                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="200" data-delay="2000">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner1.jpg" alt="kenburns1" data-bgposition="left bottom" data-kenburns="on" data-duration="1000" data-ease="Power4.easeInOut" data-bgfit="200" data-bgfitend="100" data-bgpositionend="center top">
                        <div class="tp-caption largepinkbg customin customout tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="bottom" data-voffset="-70"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="100"
                             data-start="1500"
                             data-easing="Back.easeOut"
                             data-endspeed="100"
                             data-endeasing="Back.easeIn"
                             style="z-index: 4">
                            <strong>Floor Tiles</strong> to feel luxurious
                        </div>

                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="flyin" data-slotamount="1" data-masterspeed="500">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner2.jpg" alt="kenburns2" data-bgposition="left bottom" data-kenburns="on" data-duration="5000" data-ease="Power0.easeIn" data-bgfit="120" data-bgfitend="100" data-bgpositionend="right top">

                        <div class="tp-caption largeblackbg lfl customout tp-resizeme"
                             data-x="100"
                             data-y="bottom" data-voffset="-142"
                             data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="1300"
                             data-easing="Power4.easeInOut"
                             data-endspeed="100"
                             style="z-index: 2">
                            Wall Tiles to feel luxurious
                        </div>

                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="cube" data-slotamount="6" data-masterspeed="100" data-delay="3000">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner3.jpg" alt="kenburns11" data-bgposition="left center" data-kenburns="on" data-duration="5000" data-ease="Linear.easeNone" data-bgfit="115" data-bgfitend="115" data-bgpositionend="right bottom">
                        <div class="tp-caption large_bg_black sfb stt tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="180"
                             data-speed="300"
                             data-start="1100"
                             data-easing="Back.easeOut"
                             data-endspeed="100"
                             style="z-index: 3">
                            PORCELAIN TILES
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption customin customout tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="bottom" data-voffset="-100"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="200"
                             data-easing="Back.easeOut"
                             data-endspeed="100"
                             style="z-index: 4">
                            <a href="treatments.html" target="_blank" class="btn btn-primary"> to feel luxurious</a>
                        </div>

                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="parallaxtoright" data-slotamount="7" data-masterspeed="600">
                        <img src="images/banner4.jpg" alt="kenburns5" data-bgposition="right bottom" data-kenburns="on" data-duration="11000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="left top">
                        <div class="tp-caption largeblackbg customin customout tp-resizeme"
                             data-x="right" data-hoffset="-60"
                             data-y="center" data-voffset="0"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="600"
                             data-start="500"
                             data-easing="Power3.easeInOut"
                             data-endspeed="300"
                             style="z-index: 2">
                            DIGITAL WALL TILES to feel luxurious
                        </div>
                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="parallaxtotop" data-slotamount="7" data-masterspeed="600" data-delay="4500">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner5.jpg" alt="kenburns9" data-bgposition="center center" data-kenburns="on" data-duration="1500" data-ease="Power4.easeInOut" data-bgfit="200" data-bgfitend="100" data-bgpositionend="center center">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption smoothcircle customin customout tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="0"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="1000"
                             data-start="0"
                             data-easing="Power4.easeInOut"
                             data-end="1000"
                             data-endspeed="300"
                             style="z-index: 2">
                            WALL TILES to feel luxurious
                        </div>
                        <!-- LAYER NR. 2 -->
                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="200" data-delay="2000">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner6.jpg" alt="kenburns1" data-bgposition="left bottom" data-kenburns="on" data-duration="1000" data-ease="Power4.easeInOut" data-bgfit="200" data-bgfitend="100" data-bgpositionend="center top">
                        <div class="tp-caption largepinkbg customin customout tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="bottom" data-voffset="-70"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="100"
                             data-start="1500"
                             data-easing="Back.easeOut"
                             data-endspeed="100"
                             data-endeasing="Back.easeIn"
                             style="z-index: 4">
                            <strong>SANITARYWARES</strong> to feel luxurious
                        </div>

                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="flyin" data-slotamount="1" data-masterspeed="500">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner7.jpg" alt="kenburns2" data-bgposition="left bottom" data-kenburns="on" data-duration="5000" data-ease="Power0.easeIn" data-bgfit="120" data-bgfitend="100" data-bgpositionend="right top">

                        <div class="tp-caption largeblackbg lfl customout tp-resizeme"
                             data-x="100"
                             data-y="bottom" data-voffset="-142"
                             data-customout="x:0;y:180;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500"
                             data-start="1300"
                             data-easing="Power4.easeInOut"
                             data-endspeed="100"
                             style="z-index: 2">
                            BATH FITTINGS to feel luxurious
                        </div>

                    </li>


                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!-- /REVOLUTION SLIDER -->


        <section class="container text-center" style="background-image:url(images/world-map.png)">
            <h1 class="text-center">
                We are leading exporter of<br />
                <span class="lead">Ceramic Wall Tiles, Floor Tiles, Sanitaryware!</span>
            </h1>
            <p class="lead">
                We are exporter of various construction material and have extensive range of tiles like Ceramic Wall Tiles, Ceramic Floor Tiles, Polished Porcelain Tiles, Border Tiles and sanitary ware etc. we provide quality products, packaging, on schedule transportation, etc.
            </p>
        </section>


      <section><div class="divider" style="background-color:#db9b15">
            <i class="fa fa-leaf"></i>
        </div></section>  


        <!-- PORTFOLIO -->
        <section id="portfolio" class="special-row">
            <div class="container">
                <br />
                <br />
                <h2>Product <strong>Categories</strong></h2>

                <!-- loader -->
                <span class="js_loader"><i class="fa fa-asterisk fa-spin"></i>LOADING</span>


                <div class="row">

                    <ul class="sort-destination isotope" data-sort-id="isotope-list">

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp1.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp1.jpg" height="260" alt="" data-animation="fadeInLeft">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>WALL TILES</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp2.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp2.jpg" height="260" alt="" data-animation="fadeInLeft">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>FLOOR TILES</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp3.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp3.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>PORCLEAN TILES</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp4.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp4.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>DIGITAL WALL TILES</h4>
                                </div>
                            </div>
                        </li>

                    </ul>

                    <ul class="sort-destination isotope" data-sort-id="isotope-list">

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp5.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp5.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>DIGITAL FLOOR TILES</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp6.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp6.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>DIGITAL FLOOR GVT</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp7.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp7.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>SANITARY WARES</h4>
                                </div>
                            </div>
                        </li>

                        <li class="isotope-item col-sm-6 col-md-3 development">
                            <!-- item -->
                            <div class="item-box">
                                <figure>
                                    <a class="item-hover lightbox" href="images/temp8.jpg" data-plugin-options='{"type":"image"}'>
                                        <span class="overlay color2"></span>
                                        <span class="inner">
                                            <span class="block fa fa-plus fsize20"></span>
                                            <strong>VIEW</strong> IMAGE
                                        </span>
                                    </a>
                                    <img class="img-responsive" src="images/temp8.jpg" height="260" alt="">
                                </figure>
                                <div class="item-box-desc">
                                    <h4>OTHERS</h4>
                                </div>
                            </div>
                        </li>

                    </ul>

                </div>

            </div>
        </section>
        <!-- /PORTFOLIO -->
        <!--<section class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Welcome To Relief Dental Clinic</h2>
                    <p>A gorgeous smile is contagious! Your smile conveys health, energy and vitality to others. Our patients know that an attractive smile is important., At the end of the day, it's all about YOU. That's why we pride ourselves in providing both general and cosmetic dental care for everyone, from kids to grand-parents.</p>
                    <h4>Opening Hours</h4>
                    <div class="row">
                        <div class="col-md-4">
                            Monday - Saturday <br />
                            Sunday <br />
                        </div>
                        <div class="col-md-4">
                            10 AM – 2 PM, 6 PM – 9 PM<br />
                            Closed<br />
                        </div>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="owl-carousel controlls-over" data-plugin-options='{"items": 1, "singleItem": true, "navigation": true, "pagination": true, "transitionStyle":"fadeUp"}'>
                        <div>
                            <img alt="" class="img-responsive" src="assets/images/demo/home/medical_slide_2.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-responsive" src="assets/images/demo/home/medical_slide_1.jpg">
                        </div>
                    </div>

                </div>
            </div>
        </section>



        <div class="divider">
            <i class="fa fa-leaf"></i>
        </div>


        <section class="container">
            <div class="row text-center">

                <div class="col-md-4 col-sm-6 col-xs-12 margin-top30">
                    <figure>
                        <img class="img-responsive" src="assets/images/demo/home/medical_3.jpg" alt="" />
                    </figure>

                    <h3>Preventive Care</h3>
                    <p>Preventive dentistry Care for your teeth to keep them healthy. Helping to avoid cavities, gum disease, enamel wear, and more.</p>
                    <a href="#" class="btn btn-primary btn-xs">READ MORE</a>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 margin-top30">
                    <figure>
                        <img class="img-responsive" src="assets/images/demo/home/medical_2.jpg" alt="" />
                    </figure>

                    <h3>Restorative Service</h3>
                    <p>We to help restore your smile. Our restorative services include fillings, crowns, bridges, implants, root canal treatment, dentures etc</p>
                    <a href="#" class="btn btn-primary btn-xs">READ MORE</a>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 margin-top30">
                    <figure>
                        <img class="img-responsive" src="assets/images/demo/home/medical_1.png" alt="" />
                    </figure>

                    <h3>Cosmetic Treatments</h3>
                    <p>Cosmetic Treatments to straighten, lighten, reshape and repair teeth. Cosmetic treatments include veneers, tooth-coloured fillings, implants and tooth whitening.</p>
                    <a href="#" class="btn btn-primary btn-xs">READ MORE</a>
                </div>

            </div>
        </section>-->

       
        <div class="divider" style="background-color:#db9b15">
            <i class="fa fa-leaf"></i>
        </div>
        <br />
        <!-- PARTNERS -->
        <section class="container">

            <div class="owl-carousel" data-plugin-options='{"items": 5, "singleItem": false, "autoPlay": true, "navigation": false, "pagination": false}'>
                
                <div>
                    <img class="img-responsive" src="images/lagoa-logo.jpg" alt="" style="margin-left: 220%;">
                </div>
            </div>
            
        </section>
        <!-- /PARTNERS -->
        
        <!-- PARALLAX -->
			<section class="parallax delayed margin-footer" data-stellar-background-ratio="0.7" style="background-image: url('assets/images/demo/home/church_parallax.jpg');">
				<span class="overlay"></span>

				<div class="container">

					<div class="row">
						<!-- left content -->
						<div class="col-md-7 animation_fade_in">
							<h2>Punjani  <strong>Exports</strong></h2>
							<p class="lead">
								All our efforts are to anticipate our client’s requirements and to exceed their expectations by frequently improving our standard of products and always provide quality service to our clients for a long term relationship.<br />
								
							</p>
							<p>
								  We test our products on various parameters, so as to eliminate the scope of flaws and defects.
							</p>


							<!-- Countdown -->
							
							<!-- /Countdown -->

						</div>

						<!-- right image -->
						<div class="col-md-5 animation_fade_in">
							<!-- right image , optional -->
						</div>
					</div>
				</div>
			</section>
			<!-- PARALLAX -->
    </div>

        <!-- /PARTNERS -->

  <%--  </div>--%>

</asp:Content>

