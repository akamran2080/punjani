﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper" style="padding-top: 111px;">
        <div id="shop">

            <!-- PAGE TITLE -->
            <header id="page-title">
                <div class="container">
                    

                    <ul class="breadcrumb">
                        <li><a href="Default.aspx"></a></li>
                        <h1>Elevation Luster Ordinary Series</h1>
                        <li class="active"></li>
                    </ul>
                </div>
            </header>

            <section class="container">
                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/333.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/333.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 341</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/341.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/341.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 342</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/342.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/342.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 342</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/412.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/412.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 412</h5>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    <aside class="col-md-4">

                        <h3>Available Series200 X 300 mm</h3>
                        <!-- h3 - have no margin-top -->
                        <ul class="nav nav-list">
                            <li><a href="SPblack.aspx"><i class="fa fa-circle-o"></i>SP Black Luster Orindary Series</a></li>
                            <li><a href="woodenordinary.aspx"><i class="fa fa-circle-o"></i>Wooden Ordinary and Luster Series</a></li>
                            <li><a href="RedBrown.aspx"><i class="fa fa-circle-o"></i>Red Brown Luster Ordinary Series</a></li>
                            <li><a href="Ivoryluster.aspx"><i class="fa fa-circle-o"></i>Ivory Luster Ordinary Series</a></li>
                            <li><a href="WhiteLuster.aspx"><i class="fa fa-circle-o"></i>White Luster Ordinary Series</a></li>
                            <li><a href="ElevationLuster.aspx"><i class="fa fa-circle-o"></i>Elevation Luster Ordinary Series</a></li>
                         
                        </ul>
                       <%-- <br />
                        <p class="lead"><i class="fa fa-photo"></i><a href="Products.aspx">&nbsp;Product Concepts</a></p>
                        <p class="lead"><i class="fa fa-calculator"></i><a href="TilesCalc.aspx">&nbsp;Tiles Calculator</a></p>--%>

                        <!-- SHOP FILTERS -->

                        <!-- /SHOP FILTERS -->
                        
                    </aside>

                </div>

                <div class="row">

                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/631.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/631.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 631</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/635.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/635.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 635</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/636.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/636.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : 636</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-11.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-11.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-11</h5>

                            </div>
                        </div>
                    </div>
                    <!-- items -->
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-12.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-12.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-12</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-13.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-13.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-13</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-14.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-14.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-14</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-4111.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-4111.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-4111</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-50.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-50.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-50</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-51.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-51.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-51</h5>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <!-- item 6 -->
                        <div class="item-box">
                            <figure>
                                <a class="item-hover lightbox" href="200x300/E-80.jpg" data-plugin-options='{"type":"image"}'>
                                    <span class="overlay color2"></span>
                                    <span class="inner">
                                        <span class="block fa fa-plus fsize20"></span>
                                        <strong>Wall</strong> Tiles
                                    </span>
                                </a>
                                <img class="img-responsive" src="200x300/E-80.jpg" alt="">
                            </figure>
                            <div class="item-box-desc">
                                <h5>Product ID : E-80</h5>

                            </div>
                        </div>
                    </div>




                    <!-- items -->


                    <!-- PAGINATION -->

                    <!-- /PAGINATION -->



                    

                </div>
                <!-- /row -->

            </section>

        </div>
</asp:Content>

